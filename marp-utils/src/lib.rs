/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

//! # Util functions for the marp vulkan wrapper
//! The util module provides several funtions to shortcut common worloads with the api.
//! This currently includes creating an device, a gfx queue and a Surface for a given window, and creating an test bed application which can render to a simple surface.
//! On how to use the latter one, have  a look at the examples directory.
use marp::miscellaneous::Debugging;
use marp_surface_winit::winit::{dpi::PhysicalSize, platform::run_return::EventLoopExtRunReturn};
#[cfg(all(unix, not(target_os = "android")))]
use winit::platform::unix::EventLoopWindowTargetExtUnix;

use marp::ash::vk;
use marp::command_buffer::*;
use marp::device::*;
use marp::image::*;
use marp::pipeline::*;
use marp::shader::*;
use marp::swapchain::surface::Surface;
use marp::*;
use marp_surface_winit::winit;
use marp_surface_winit::WinitSurface;
use winit::{event::*, event_loop::*};

use std::sync::Arc;
use std::time::{Duration, Instant};
use std::u64;

///Controlls how many frames we wnat to have in flight concurrently
pub const FRAMES_IN_FLIGHT: usize = 2;

#[derive(Debug)]
pub enum UtilError {
    CouldNotCreateInstance,
    NoPhysicalDevice,
    NoGraphicsQueueOnDevice,
    FailedToCreateGraphicsQueue,
}

#[cfg(all(unix, not(target_os = "android")))]
pub fn is_wayland(events_loop: &winit::event_loop::EventLoop<()>) -> bool {
    events_loop.is_wayland()
}

#[cfg(windows)]
pub fn is_wayland(events_loop: &winit::event_loop::EventLoop<()>) -> bool {
    false
}

pub fn create_device_and_gfx_queue(
    window: &winit::window::Window,
    events_loop: &winit::event_loop::EventLoop<()>,
) -> Result<
    (
        Arc<Device>,
        Arc<Queue>,
        Arc<dyn swapchain::surface::Surface + Send + Sync>,
    ),
    UtilError,
> {
    //Create an instance
    let mut app_info = miscellaneous::AppInfo::default("MyApp".to_string());
    app_info.set_api_version(miscellaneous::Version::new(1, 2, 0));
    let mut extensions = miscellaneous::InstanceExtensions::presentable();
    //Check if we can disable the wayland extension by querying the window for wayland
    if !is_wayland(events_loop) {
        println!("Not using wayland!");
        extensions.wayland_surface = false;
    }

    //Should load the debug marker extension anyways
    let layer = miscellaneous::InstanceLayer::debug_layers();

    let instance = match instance::Instance::new(
        Some(app_info),
        Some(extensions),
        Some(layer),
        Some(Debugging::errors()),
    ) {
        Ok(inst) => inst,
        Err(er) => {
            println!("failed to create instance: {}", er);
            return Err(UtilError::CouldNotCreateInstance);
        }
    };

    let surface: Arc<dyn Surface + Send + Sync> =
        WinitSurface::new(instance.clone(), &window, &events_loop)
            .expect("Failed to create surface!");

    //Now find as a physical device we can use
    let p_devices = physical_device::PhysicalDevice::find_physical_device(instance.clone())
        .expect("failed to find physical devices");
    if p_devices.len() < 1 {
        return Err(UtilError::NoPhysicalDevice);
    }

    //find the physical device that supports presentation on out surface.
    let (physical_device, _index) = p_devices
        .into_iter()
        .filter_map(|pdev| match pdev.find_present_queue_family(&surface) {
            Ok(queue_idx) => {
                println!("Found suitable physical device");
                Some((pdev, queue_idx))
            }
            Err(er) => {
                println!("Could not find presentable surface for dev: {:?}", er);
                None
            }
        })
        .nth(0)
        .expect("failed to find suitable device at all");

    //Check for a graphics capable queue family.
    for (i, qf) in physical_device.get_queue_families().iter().enumerate() {
        println!(
            "Fami[{}]: \n\tgraphics: {}\n\tcompute: {}\n\ttransfer: {}",
            i,
            qf.get_queue_type().graphics,
            qf.get_queue_type().compute,
            qf.get_queue_type().transfer
        );
    }

    let queue_create_vec: Vec<(queue::QueueFamily, f32)> = physical_device
        .get_queue_families()
        .into_iter()
        .filter_map(|q| {
            if q.get_queue_type().graphics {
                Some((*q, 1.0 as f32))
            } else {
                None
            }
        })
        .collect();

    //Check if we got at least one queue
    if queue_create_vec.len() < 1 {
        return Err(UtilError::NoGraphicsQueueOnDevice);
    }

    let mut needed_device_ext = vec![miscellaneous::DeviceExtension::new(
        "VK_KHR_swapchain".to_string(),
        1,
    )];

    if !is_wayland(events_loop) {
        //Check if we can enable the debug markers
        for e in physical_device.get_extensions() {
            if e.get_name() == "VK_EXT_debug_marker".to_string() {
                println!("Can load Debug marker!");
                needed_device_ext.push(miscellaneous::DeviceExtension::new(
                    "VK_EXT_debug_marker".to_string(),
                    1,
                ));
            }
        }
    }

    //Try to create a device with debug markers

    let (device, queues) = Device::new(
        instance.clone(),
        physical_device.clone(),
        queue_create_vec,
        Some(&needed_device_ext),
        None,
        //Some(*physical_device.get_features())
    )
    .expect("failed to find Device and its queues");
    if queues.len() < 1 {
        return Err(UtilError::FailedToCreateGraphicsQueue);
    }

    Ok((
        device,
        queues
            .get(0)
            .expect("Could not pop graphics queue from queues vec!")
            .clone(),
        surface,
    ))
}

///A Test bed which opens a window and manages it's own render target as well as depth target image.
/// It is resizeable and the render function can be changed.
pub struct RenderBed {
    pub device: Arc<Device>,
    pub queue: Arc<Queue>,
    pub events_loop: Option<winit::event_loop::EventLoop<()>>,
    pub window: winit::window::Window,

    pub swapchain: Arc<swapchain::Swapchain>,
    pub swapchain_format: ash::vk::SurfaceFormatKHR,
    pub depth_format: ash::vk::Format,
    pub depth_image: Option<Arc<image::Image>>,

    pub render_pass: Arc<framebuffer::RenderPass>,
    pub framebuffers: Option<Vec<Arc<framebuffer::Framebuffer>>>,

    pub sem_present_complete: Vec<Arc<sync::Semaphore>>,
    pub sem_render_complete: Vec<Arc<sync::Semaphore>>,
    pub in_light_fences: [Option<sync::QueueFence>; FRAMES_IN_FLIGHT],

    pub command_pool: Arc<command_buffer::CommandBufferPool>,
    pub command_buffers: Vec<Arc<command_buffer::CommandBuffer>>,

    pub vertex_shader: Arc<shader::ShaderModule>,
    pub fragment_shader: Arc<shader::ShaderModule>,

    pub pipeline: Arc<pipeline::GraphicsPipeline>,

    //keeps track of which command buffer / image should be given back to the caller
    inner_count: usize,
    //Holds information about frame times
    frames: usize,
    last_upd: Instant,
}

impl RenderBed {
    pub fn new(
        device: Arc<Device>,
        queue: Arc<Queue>,
        surface: Arc<dyn swapchain::surface::Surface + Send + Sync>,
        window: winit::window::Window,
        events_loop: winit::event_loop::EventLoop<()>,
        vertex_shader_path: &str,
        fragment_shader_path: &str,
        descriptor_set_layouts: Vec<vk::DescriptorSetLayout>,
        push_const_layouts: Vec<vk::PushConstantRange>,
        vertex_state: pipeline::VertexInputState,
    ) -> Self {
        //Find a depth and color format
        let swapchain_format = surface
            .get_supported_formats(device.get_physical_device())
            .expect("Failed to get surface formats")
            .into_iter()
            .nth(0)
            .expect("failed to get first swapchain format");
        let depth_format = device
            .get_useable_format(
                vec![
                    vk::Format::D32_SFLOAT_S8_UINT,
                    vk::Format::D24_UNORM_S8_UINT,
                    vk::Format::D16_UNORM_S8_UINT,
                ],
                &image::ImageUsage {
                    depth_attachment: true,
                    input_attachment: true,
                    ..Default::default()
                },
                ash::vk::ImageTiling::OPTIMAL,
            )
            .expect("Could not find a depth format");

        let inner_extent = window.inner_size();
        let swapchain_extent = ash::vk::Extent2D::builder()
            .width(inner_extent.width as u32)
            .height(inner_extent.height as u32)
            .build();

        //Create swapchain
        let swapchain = swapchain::Swapchain::new(
            device.clone(),
            surface,
            swapchain_extent,
            Some(swapchain_format),
            Some(2),
            Some(ash::vk::PresentModeKHR::IMMEDIATE),
            Some(ImageUsage {
                color_attachment: true,
                ..Default::default()
            }),
        )
        .expect("Failed to create swapchain!");

        //Turn into present
        swapchain
            .images_to_present_layout(queue.clone())
            .wait(u64::MAX)
            .unwrap();

        //create forst depth image. This might be recreated if the swapchain extend changes.
        let depth_image_info = image::ImageInfo::new(
            image::ImageType::Image2D {
                width: swapchain_extent.width,
                height: swapchain_extent.height,
                samples: 1,
            },
            depth_format,
            None,
            Some(image::MipLevel::Specific(1)),
            image::ImageUsage {
                depth_attachment: true,
                input_attachment: true,
                ..Default::default()
            },
            memory::MemoryUsage::GpuOnly,
            None,
        );

        let depth_image = image::Image::new(
            device.clone(),
            depth_image_info,
            buffer::SharingMode::Exclusive,
        )
        .expect("Failed to create depth image!");

        println!("Has {} images", swapchain.get_images().len());

        //Setup the renderpass we want to use while rendering the triangle
        let attachment_descs = vec![
            //The color attachment
            framebuffer::AttachmentDescription::new(
                swapchain.get_images()[0].image_type().sample_count_flag(),
                swapchain.get_images()[0].format(),
                ash::vk::AttachmentStoreOp::STORE,
                ash::vk::AttachmentLoadOp::CLEAR,
                None, // no stencil store
                None, // no stencil load
                None, //No initial layout needed since we clear anyways
                ash::vk::ImageLayout::PRESENT_SRC_KHR,
            ),
            //The depth image
            depth_image.to_attachment_description(
                ash::vk::AttachmentLoadOp::CLEAR,
                ash::vk::AttachmentStoreOp::DONT_CARE,
            ),
        ];

        //This are the subpasses this renderpass goes through. However, this is easy in a triangle example.
        let render_pass = framebuffer::RenderPass::new(attachment_descs)
            .add_subpass(vec![
                framebuffer::AttachmentRole::OutputColor,
                framebuffer::AttachmentRole::DepthStencil,
            ])
            .add_dependency(framebuffer::SubpassDependency::new(
                ash::vk::SUBPASS_EXTERNAL,                            //from external pass
                0,                                                    //To first and only subpass
                ash::vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT, //When color output has finished on src
                ash::vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT, //Color output can start on this one
                ash::vk::AccessFlags::empty(), //Don't know access type of source
                ash::vk::AccessFlags::COLOR_ATTACHMENT_READ
                    | ash::vk::AccessFlags::COLOR_ATTACHMENT_WRITE, //We need color read and write support
            ))
            .add_dependency(framebuffer::SubpassDependency::new(
                0,
                ash::vk::SUBPASS_EXTERNAL,
                ash::vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT,
                ash::vk::PipelineStageFlags::BOTTOM_OF_PIPE,
                ash::vk::AccessFlags::COLOR_ATTACHMENT_READ
                    | ash::vk::AccessFlags::COLOR_ATTACHMENT_WRITE, //Don't know access type of source
                ash::vk::AccessFlags::empty(),
            ))
            .build(device.clone())
            .expect("Failed to build renderpass!");

        //setup the frambuffers, one per swapchain image, has to be recreated if extent changes.
        let mut framebuffers = Vec::new();
        for idx in 0..swapchain.get_images().len() {
            framebuffers.push(
                framebuffer::Framebuffer::new(
                    device.clone(),
                    render_pass.clone(),
                    vec![swapchain.get_images()[idx].clone(), depth_image.clone()],
                )
                .expect("Failed to create Framebuffer!"),
            );
        }

        //Setup the command pool and allocate a command buffer for each frame on the swapchain
        let command_pool = command_buffer::CommandBufferPool::new(
            device.clone(),
            queue.clone(),
            ash::vk::CommandPoolCreateFlags::RESET_COMMAND_BUFFER,
        )
        .expect("Failed to create command pool");

        let command_buffers = command_pool
            .alloc(swapchain.get_images().len(), false)
            .expect("Failed to allocate command buffers"); //Allocate a primary command buffer for each frame/swapchain image.

        //Load Shaders
        let vertex_shader = ShaderModule::new_from_spv(device.clone(), vertex_shader_path)
            .expect("Failed to load vertex shader");
        let fragment_shader = ShaderModule::new_from_spv(device.clone(), fragment_shader_path)
            .expect("Failed to load fragment shader");

        let vertex_stage = vertex_shader.to_stage(Stage::Vertex, "main");
        let fragment_stage = fragment_shader.to_stage(Stage::Fragment, "main");

        //This describes how the input (the vertices) should be interpreted
        let input_assembly_state = pipeline::InputAssemblyState {
            topology: vk::PrimitiveTopology::TRIANGLE_LIST,
            restart_enabled: false,
        };

        //TODO raster_info
        let raster_info = pipeline::RasterizationState {
            depth_clamp_enabled: false,
            rasterizer_discard_enabled: false,
            polygone_mode: vk::PolygonMode::FILL,
            cull_mode: vk::CullModeFlags::NONE, //No culling for now
            front_face: vk::FrontFace::COUNTER_CLOCKWISE,
            line_width: None,
            depth_bias: None, //No depth bias for now
        };

        //TODO multisample_state
        let multisample_state = pipeline::MultisampleState {
            sample_count: vk::SampleCountFlags::TYPE_1,
            sample_shading: false,
            min_sample_shading: 1.0,
            alpha_to_coverage: false,
            alpha_to_one: false,
        };
        //TODO depth_stencil_state
        let depth_stencil_state = pipeline::DepthStencilState {
            depth_test_enabled: true,
            depth_write_enabled: true,
            depth_compare_op: vk::CompareOp::LESS,
            stencil_test: pipeline::StencilTestState::NoTest,
            depth_bounds: None,
        };
        //TODO color_blend
        let color_blend_state = pipeline::ColorBlendState::new(
            Some([0.0, 0.0, 0.0, 0.0]),
            None,
            vec![
                //Our Color attachment
                ColorBlendAttachmentState::Blending {
                    //selects which blend factor is used to determine the source factors
                    src_color_blend_factor: vk::BlendFactor::SRC_ALPHA,
                    //selects which blend factor is used to determine the destination factors
                    dst_color_blend_factor: vk::BlendFactor::ONE_MINUS_SRC_ALPHA,
                    // set the blend operation used to write to this attachment
                    color_blend_op: vk::BlendOp::ADD,
                    // selects which blend factor is used to determine the source factor
                    src_alpha_blend_factor: vk::BlendFactor::ONE,
                    // selects which blend factor is used to determine the destination factor
                    dst_alpha_blend_factor: vk::BlendFactor::ZERO,
                    //the blending operation for the alpha channel.
                    alpha_blend_op: vk::BlendOp::ADD,
                    //Mask which describes which channels are enabled for writing.
                    color_write_mask: vk::ColorComponentFlags::all(),
                },
            ],
        );

        //Create pipeline
        let pipeline_state = pipeline::PipelineState::new(
            vertex_state,
            input_assembly_state,
            None,
            pipeline::ViewportState::new(pipeline::ViewportMode::Dynamic(1)),
            raster_info,
            multisample_state,
            depth_stencil_state,
            Some(color_blend_state),
        );

        let pipeline_layout = pipeline::PipelineLayout::new(
            device.clone(),
            descriptor_set_layouts,
            push_const_layouts,
        )
        .expect("Failed to create GraphicsPipelineLayout");

        let pipeline = pipeline::GraphicsPipeline::new(
            device.clone(),
            vec![vertex_stage.clone(), fragment_stage.clone()],
            pipeline_state,
            pipeline_layout,
            render_pass.clone(),
            0, //Use subpass 0
        )
        .expect("Failed to create Graphics pipeline");

        let mut sem_present_complete = Vec::new();
        for _ in 0..FRAMES_IN_FLIGHT {
            sem_present_complete.push(
                sync::Semaphore::new(device.clone()).expect("Failed to create present semaphore"),
            );
        }

        let mut sem_render_complete = Vec::new();
        for _ in 0..FRAMES_IN_FLIGHT {
            sem_render_complete.push(
                sync::Semaphore::new(device.clone()).expect("Failed to create render semaphore"),
            );
        }

        RenderBed {
            device: device.clone(),
            queue,
            events_loop: Some(events_loop),
            window,

            swapchain,
            swapchain_format,
            depth_format,
            depth_image: Some(depth_image),

            render_pass,
            framebuffers: Some(framebuffers),

            sem_present_complete,
            sem_render_complete,
            in_light_fences: [None, None],

            command_pool,
            command_buffers,

            vertex_shader,
            fragment_shader,

            pipeline,

            inner_count: 0,
            frames: 0,
            last_upd: Instant::now(),
        }
    }

    ///Has to be executed if the swapchain extend changes.
    fn resize(&mut self, new: PhysicalSize<u32>) {
        self.device.wait_idle().expect("Could not wait for device!");
        println!("Resizing to {:?}", new);
        //Recreate the swapchian
        //let inner_extent = self.window.inner_size();
        let dpi = self.window.scale_factor();
        let swapchain_extent = ash::vk::Extent2D::builder()
            .width((new.width as f64 * dpi) as u32)
            .height((new.height as f64 * dpi) as u32)
            .build();

        self.swapchain.recreate(swapchain_extent);

        let submit_fence = self.swapchain.images_to_present_layout(self.queue.clone());
        submit_fence
            .wait(u64::MAX)
            .expect("Failed to wait for layout transition");

        //Update extent with the actual created extent
        let swapchain_extent = self.swapchain.get_images()[0].extent();

        //now recreate the depth image and the frame buffers.
        let depth_image_info = image::ImageInfo::new(
            image::ImageType::Image2D {
                width: swapchain_extent.width,
                height: swapchain_extent.height,
                samples: 1,
            },
            self.depth_format,
            None,
            Some(image::MipLevel::Specific(1)),
            image::ImageUsage {
                depth_attachment: true,
                input_attachment: true,
                ..Default::default()
            },
            memory::MemoryUsage::GpuOnly,
            None,
        );

        let depth_image = image::Image::new(
            self.device.clone(),
            depth_image_info,
            buffer::SharingMode::Exclusive,
        )
        .expect("Failed to create depth image!");

        //setup the frambuffers, one per swapchain image, has to be recreated if extent changes.
        let mut framebuffers = Vec::new();
        for image in self.swapchain.get_images() {
            framebuffers.push(
                framebuffer::Framebuffer::new(
                    self.device.clone(),
                    self.render_pass.clone(),
                    vec![image, depth_image.clone()],
                )
                .expect("Failed to create Framebuffer!"),
            );
        }

        let _old_depth_image = self
            .depth_image
            .take()
            .expect("Failed to recreate depth image after resize!");
        self.depth_image = Some(depth_image);

        let _old_framebuffers = self
            .framebuffers
            .take()
            .expect("Failed to take old frame buffers!");
        self.framebuffers = Some(framebuffers);

        self.swapchain
            .images_to_present_layout(self.queue.clone())
            .wait(std::u64::MAX)
            .expect("Failed to wait for swapchain!");
    }

    pub fn upd_fps(&mut self) {
        if self.last_upd.elapsed() > Duration::from_secs(1) {
            println!(
                "avg frame time: {}ms / {}fps",
                ((self.last_upd.elapsed().as_millis() as f64) / (self.frames as f64)) as usize,
                self.frames
            );

            self.frames = 0;
            self.last_upd = Instant::now();
        } else {
            self.frames += 1;
        }
    }

    ///Starts the render loop. `render_job` is a closure that gets a already started command buffer. You can add your draw calls and return it afterwards for it to be presented.
    pub fn start<J>(mut self, mut render_job: J)
    where
        J: FnMut((usize, Arc<command_buffer::CommandBuffer>)) -> Arc<command_buffer::CommandBuffer>,
        J: 'static,
    {
        let mut should_close = false;
        let mut should_resize = None;

        let mut eloop = self.events_loop.take().expect("Could not take event loop!");

        loop {
            eloop.run_return(|event, _, control_flow| {
                *control_flow = winit::event_loop::ControlFlow::Wait;
                match event {
                    Event::WindowEvent { event, .. } => {
                        match event {
                            winit::event::WindowEvent::KeyboardInput { input, .. } => {
                                match input.virtual_keycode {
                                    Some(winit::event::VirtualKeyCode::Escape) => {
                                        should_close = true
                                    }
                                    _ => {} //Do not close
                                }
                            }
                            winit::event::WindowEvent::CloseRequested => {
                                should_close = true;
                                *control_flow = winit::event_loop::ControlFlow::Exit;
                            }
                            winit::event::WindowEvent::Resized(a) => should_resize = Some(a),
                            _ => {}
                        }
                    }
                    Event::MainEventsCleared => {
                        *control_flow = ControlFlow::Exit;
                    }
                    _ => {} //All other events
                }
            });

            if should_close {
                self.device.wait_idle().expect("Failed to wait for idle!");
                return;
            }

            if let Some(new_size) = should_resize.take() {
                self.resize(new_size);
            }

            self.upd_fps();
            //Shedulre redraw
            let new = self.start_cb();
            let finished_cb = render_job(new);
            self.end(finished_cb);
        }
    }

    fn start_cb(&mut self) -> (usize, Arc<command_buffer::CommandBuffer>) {
        //Now start the current Command buffer by giving it the new viewport and
        //scissors, then add the recording commands. Finally end it and execute.
        self.inner_count += 1;
        let index = self.inner_count % self.command_buffers.len();

        //Check if we can start subitting this frame or if this slot is still in flight
        match self.in_light_fences[self.inner_count % FRAMES_IN_FLIGHT].take() {
            Some(fence) => {
                fence
                    .wait(u64::MAX)
                    .expect("Failed to wait for frame in flight");
            }
            None => {
                println!(
                    "No Frame in flight on slot {}",
                    self.inner_count % FRAMES_IN_FLIGHT
                );
            }
        }

        let cb = self.command_buffers[index].clone();
        cb.reset().expect("Failed to reset command buffer!");

        cb.begin_recording(false, false, false, None)
            .expect("Failed to start command buffer recording!");

        let clear_values = vec![
            vk::ClearValue {
                color: vk::ClearColorValue {
                    float32: [0.0, 0.0, 0.0, 1.0],
                },
            },
            vk::ClearValue {
                depth_stencil: vk::ClearDepthStencilValue {
                    depth: 1.0,
                    stencil: 0,
                },
            },
        ];

        let fb = if let Some(ref fb) = self.framebuffers {
            fb[index].clone()
        } else {
            panic!("Could not get framebuffer!");
        };

        cb.cmd_begin_render_pass(
            self.render_pass.clone(),
            fb,
            vk::Rect2D {
                offset: vk::Offset2D { x: 0, y: 0 },
                extent: self.swapchain.get_extent(),
            },
            clear_values,
            vk::SubpassContents::INLINE,
        )
        .expect("Failed to start renderpass");

        cb.cmd_bind_pipeline(vk::PipelineBindPoint::GRAPHICS, self.pipeline.clone())
            .expect("Failed to bind pipeline!");

        cb.cmd_set_viewport(vec![vk::Viewport {
            x: 0.0,
            y: 0.0,
            width: self.swapchain.get_extent().width as f32,
            height: self.swapchain.get_extent().height as f32,
            min_depth: 0.0,
            max_depth: 1.0,
        }])
        .expect("Failed to set viewport");

        cb.cmd_set_scissors(vec![vk::Rect2D {
            offset: vk::Offset2D { x: 0, y: 0 },
            extent: self.swapchain.get_extent(),
        }])
        .expect("Failed to set scissors!");

        (self.inner_count % FRAMES_IN_FLIGHT, cb)
    }

    pub fn end(&mut self, cb: Arc<command_buffer::CommandBuffer>) {
        cb.cmd_end_render_pass().expect("failed to end renderpass!");
        cb.end_recording().expect("Failed to end cb recording!");

        let submit_image_idx = match self.swapchain.acquire_next_image(
            u64::MAX,
            self.sem_present_complete[self.inner_count % FRAMES_IN_FLIGHT].clone(),
        ) {
            Ok(idx) => idx,
            Err(_) => {
                panic!("Could not get next image index");
            }
        };

        //TODO remove double sems
        let sem = self.sem_present_complete[self.inner_count % FRAMES_IN_FLIGHT].clone();

        let render_fence = self
            .queue
            .queue_submit(vec![queue::SubmitInfo::new(
                vec![(sem, vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT)], //wait for this
                vec![cb],
                vec![self.sem_render_complete[self.inner_count % FRAMES_IN_FLIGHT].clone()],
            )])
            .expect("Failed to submit work to queue!");

        //wait for the work
        //render_fence.wait(u64::MAX).expect("Failed to wait for the renderer!");
        //Add this frame to the current slot
        if self.in_light_fences[self.inner_count % FRAMES_IN_FLIGHT].is_some() {
            panic!("There was a frame in flight which should have been waited for!")
        } else {
            self.in_light_fences[self.inner_count % FRAMES_IN_FLIGHT] = Some(render_fence);
        }

        match self.swapchain.queue_present(
            self.queue.clone(),
            vec![self.sem_render_complete[self.inner_count % FRAMES_IN_FLIGHT].clone()],
            submit_image_idx,
        ) {
            Ok(_) => {}
            Err(_er) => {
                println!("Could not present!");
            }
        }
    }
}
