/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#[macro_export]
/// Calculates the offset of `field` in a struct `path`.
/// Can be used for instance when setting the offset of an `vk::VertexInputAttributeDescription`.
///
/// Simple offset_of macro akin to C++ offsetof
macro_rules! offset_of {
    ($base:path, $field:ident) => {{
        #[allow(unused_unsafe)]
        unsafe {
            let b: $base = core::mem::zeroed();
            (&b.$field as *const _ as isize) - (&b as *const _ as isize)
        }
    }};
}

///SeaHash based [HashMap](std::collections::HashMap).;
pub type SeaHashMap<K, V> =
    std::collections::HashMap<K, V, std::hash::BuildHasherDefault<seahash::SeaHasher>>;

///SeaHash based [HashSet](std::collections::HashSet).
pub type SeaHashSet<T> =
    std::collections::HashSet<T, std::hash::BuildHasherDefault<seahash::SeaHasher>>;
