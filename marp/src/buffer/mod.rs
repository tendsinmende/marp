/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use crate::command_buffer::*;
use crate::device::queue::*;
use crate::device::{Device, Queue};
use crate::memory::*;
use crate::sync::*;
use crate::util::SeaHashSet;

use ash;
use ash::vk::{AccessFlags, PipelineStageFlags};
use gpu_allocator::vulkan::Allocation;

use std::fmt::Formatter;
use std::sync::Arc;
use std::u64;

///Describes how a resource is shared between queues. If you use Concurrent you can use it across all queues specified, but this will be slower. Exclusive is faster
/// but you need to take care of correct resource ownership transfer.
#[derive(Clone)]
pub enum SharingMode {
    ///Can only be used on one QueueFamily at once. Ownership needs to be transferred through pipeline barriers.
    Exclusive,
    ///Can be accessed from any queue registered in here at the same time.
    Concurrent(Vec<Arc<Queue>>),
}

impl SharingMode {
    pub fn vko(&self) -> ash::vk::SharingMode {
        //Check if our concurrent setup has only one queue attached. If so convert to exclusive.
        match self {
            SharingMode::Concurrent(queues) => {
                if queues.len() > 1 {
                    ash::vk::SharingMode::CONCURRENT
                } else {
                    ash::vk::SharingMode::EXCLUSIVE
                }
            }
            SharingMode::Exclusive => ash::vk::SharingMode::EXCLUSIVE,
        }
    }
    ///Returns the queue indices which are used if the mode is `Concurrent`.
    pub fn queue_indices<'a>(&self) -> Vec<u32> {
        match self {
            SharingMode::Concurrent(queues) => {
                let mut indice_set = SeaHashSet::default();
                for q in queues {
                    indice_set.insert(q.get_family().get_family_index());
                }
                //Now turn into a vec and returnt he slice of it
                let indices: Vec<u32> = indice_set.into_iter().collect();
                indices
            }
            SharingMode::Exclusive => {
                vec![]
            }
        }
    }

    pub fn is_exclusive(&self) -> bool {
        match self {
            SharingMode::Exclusive => true,
            _ => false,
        }
    }
}

pub struct BufferUsage {
    pub transfer_src: bool,
    pub transfer_dst: bool,
    pub uniform_texel_buffer: bool,
    pub storage_texel_buffer: bool,
    pub uniform_buffer: bool,
    pub storage_buffer: bool,
    pub index_buffer: bool,
    pub vertex_buffer: bool,
    pub indirect_buffer: bool,
}

impl BufferUsage {
    ///Generates the `BufferUsageFlags`. Assumes that they are valid.
    pub fn to_usage_flags(&self) -> ash::vk::BufferUsageFlags {
        let mut flags = ash::vk::BufferUsageFlags::empty();

        if self.transfer_src {
            flags = flags | ash::vk::BufferUsageFlags::TRANSFER_SRC;
        }
        if self.transfer_dst {
            flags = flags | ash::vk::BufferUsageFlags::TRANSFER_DST;
        }
        if self.uniform_texel_buffer {
            flags = flags | ash::vk::BufferUsageFlags::UNIFORM_TEXEL_BUFFER;
        }
        if self.storage_texel_buffer {
            flags = flags | ash::vk::BufferUsageFlags::STORAGE_TEXEL_BUFFER;
        }
        if self.uniform_buffer {
            flags = flags | ash::vk::BufferUsageFlags::UNIFORM_BUFFER;
        }
        if self.storage_buffer {
            flags = flags | ash::vk::BufferUsageFlags::STORAGE_BUFFER;
        }
        if self.index_buffer {
            flags = flags | ash::vk::BufferUsageFlags::INDEX_BUFFER;
        }
        if self.vertex_buffer {
            flags = flags | ash::vk::BufferUsageFlags::VERTEX_BUFFER;
        }
        if self.indirect_buffer {
            flags = flags | ash::vk::BufferUsageFlags::INDIRECT_BUFFER;
        }
        flags
    }
    ///Returns true if the usage is a valid combination of the usage flags.
    pub fn is_valid(&self) -> bool {
        //Could not find a invalid usage to far.
        //TODO research
        true
    }
    ///Adds Access flags based on the specified usage. However, HOST_WRITE and HOST_READ as well as MEMORY_READ and MEMORY_WRITE can't be
    /// derived from the usage.
    pub fn to_access_mask(&self) -> ash::vk::AccessFlags {
        let mut flags = ash::vk::AccessFlags::empty();
        if self.indirect_buffer {
            flags = flags | ash::vk::AccessFlags::INDIRECT_COMMAND_READ;
        }

        if self.index_buffer {
            flags = flags | ash::vk::AccessFlags::INDEX_READ;
        }

        if self.vertex_buffer {
            flags = flags | ash::vk::AccessFlags::VERTEX_ATTRIBUTE_READ;
        }

        if self.uniform_buffer {
            flags = flags | ash::vk::AccessFlags::UNIFORM_READ;
        }
        //TODO only needed for loads
        if self.storage_buffer || self.uniform_texel_buffer || self.storage_texel_buffer {
            flags = flags | ash::vk::AccessFlags::SHADER_READ;
        }
        //TODO only needed for writes
        if self.storage_buffer || self.storage_texel_buffer {
            flags = flags | ash::vk::AccessFlags::SHADER_WRITE;
        }
        if self.transfer_src {
            flags = flags | ash::vk::AccessFlags::TRANSFER_READ;
        }

        if self.transfer_dst {
            flags = flags | ash::vk::AccessFlags::TRANSFER_WRITE;
        }

        flags
    }
}

///Initializes everything to false
impl Default for BufferUsage {
    fn default() -> Self {
        BufferUsage {
            transfer_src: false,
            transfer_dst: false,
            uniform_texel_buffer: false,
            storage_texel_buffer: false,
            uniform_buffer: false,
            storage_buffer: false,
            index_buffer: false,
            vertex_buffer: false,
            indirect_buffer: false,
        }
    }
}

pub struct Buffer {
    device: Arc<Device>,
    //flags: BufferCreateFlags,
    //is the current access mask used.
    access_mask: ash::vk::AccessFlags,
    size: u64,
    usage: BufferUsage,
    pub(crate) sharing_mode: SharingMode,
    #[allow(dead_code)]
    memory_usage: MemoryUsage,
    //The buffer we mean
    buffer: ash::vk::Buffer,
    memory_handle: Allocation,
}

impl std::fmt::Display for Buffer {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "
Marp Bffer{{
    size:      {},
    memory id: {:?},
    buffer id: {:?}
}}",
            self.size, self.memory_handle, self.buffer
        )
    }
}

impl Buffer {
    ///Creates a new Buffer with the `size`. If you already know your data, try to use the `from_data()` function for automatic upload of the data to the buffer.
    pub fn new(
        device: Arc<Device>,
        size: u64,
        usage: BufferUsage,
        sharing: SharingMode,
        memory_usage: MemoryUsage,
    ) -> Result<Arc<Self>, AllocationError> {
        //Create info. TODO add spares binding?
        let queue_indices = sharing.queue_indices();
        let buffer_create_info = ash::vk::BufferCreateInfo::builder()
            .size(size)
            .usage(usage.to_usage_flags())
            .sharing_mode(sharing.vko())
            .queue_family_indices(queue_indices.as_slice());

        let (buffer, allocation) = device.get_allocator_locked()?.create_buffer(
            device.vko(),
            &buffer_create_info,
            memory_usage,
            None,
        )?;

        Ok(Arc::new(Buffer {
            device,
            access_mask: ash::vk::AccessFlags::empty(),
            size: allocation.size() as u64,
            usage,
            sharing_mode: sharing,
            memory_usage,
            buffer,
            memory_handle: allocation,
        }))
    }

    /// Creates a buffer which holds `data`. Either maps directly to the buffer or uploads it via a
    /// small command buffer on the `upload_queue`.
    /// If you want finer control, use `Buffer::new()` together with the `UploadStrategy` trait.
    pub fn from_data<T: Copy + Sized>(
        device: Arc<Device>,
        upload_queue: Arc<Queue>,
        data: Vec<T>,
        mut usage: BufferUsage,
        sharing: SharingMode,
        memory_usage: MemoryUsage,
    ) -> Result<(QueueFence, Arc<Self>), AllocationError> {
        let data_size = (data.len() * std::mem::size_of::<T>()) as u64;
        //Allways set the buffers transfer src to true
        usage.transfer_dst = true;

        let target_buffer = Buffer::new(device.clone(), data_size, usage, sharing, memory_usage)?;

        //Check if we have device local or map able.
        match memory_usage {
            MemoryUsage::GpuToCpu | MemoryUsage::CpuToGpu => {
                target_buffer.map_data(data)?;
                //Return an signaled fence
                let fence = Fence::new(device.clone(), true, None)
                    .expect("Could not create signaled fence after mapping");
                Ok((fence, target_buffer))
            }
            _ => {
                //Upload via small buffer
                let fence = target_buffer.copy(upload_queue, data)?;
                Ok((fence, target_buffer))
            }
        }
    }

    pub fn vko(&self) -> &ash::vk::Buffer {
        &self.buffer
    }

    pub fn usage(&self) -> &BufferUsage {
        &self.usage
    }

    pub fn size(&self) -> u64 {
        self.size
    }

    ///Builds optimal access flags based on the usage and the memory usage of this buffer.
    /// Note that ACCESS_MEMORY_READ and ACCESS_MEMORY_WRITE are not added.
    pub fn get_optimal_access_flags(&self) -> ash::vk::AccessFlags {
        let flags = self.usage.to_access_mask();
        //TODO Check if that's enough

        /*
        if self.memory_usage.contains(ash::vk::MemoryPropertyFlags::HOST_VISIBLE){
            flags = flags | ash::vk::AccessFlags::MEMORY_READ;
        }
        if self.memory_usage.contains(ash::vk::MemoryPropertyFlags::HOST_COHERENT){
            flags = flags | ash::vk::AccessFlags::MEMORY_WRITE;
        }
        */

        flags
    }

    ///Returns the current access mask, assuming that all buffer barriers generated for this are in submitted.
    pub fn get_access_mask(&self) -> ash::vk::AccessFlags {
        self.access_mask
    }

    /// Transitions ownership of a buffer to another queue if needed.
    /// It can also change the access mask of this buffer to a new type.
    ///Via offset and size subregions of a buffer can be transitioned.
    ///When no offset is given, `0` is used, when no size is given `WHOLE_SIZE` is used.
    ///
    ///
    #[deprecated(
        note = "use `BufferMemoryBarrierBuilder` with a `PipelineBarrierBuilder` instead."
    )]
    pub fn buffer_barrier(
        &self,
        old_queue: Option<Arc<Queue>>,
        new_queue: Option<Arc<Queue>>,
        old_access_mask: Option<ash::vk::AccessFlags>,
        new_access_mask: Option<ash::vk::AccessFlags>,
        offset: Option<u64>,
        size: Option<u64>,
    ) -> ash::vk::BufferMemoryBarrier {
        let mut builder = ash::vk::BufferMemoryBarrier::builder();
        match (old_access_mask, new_access_mask) {
            (Some(oa), Some(na)) => {
                builder = builder.src_access_mask(oa).dst_access_mask(na);
            }
            (None, Some(na)) => {
                builder = builder.dst_access_mask(na);
            }
            (Some(oa), None) => {
                builder = builder.src_access_mask(oa);
            }
            (None, None) => {
                //Nothingo
            }
        }

        //Only add if we are not concurrent
        if self.sharing_mode.is_exclusive() {
            match (old_queue, new_queue) {
                (Some(old), Some(new)) => {
                    builder = builder
                        .src_queue_family_index(old.get_family().get_family_index())
                        .dst_queue_family_index(new.get_family().get_family_index());
                }
                (None, Some(new)) => {
                    builder = builder
                        .src_queue_family_index(ash::vk::QUEUE_FAMILY_IGNORED)
                        .dst_queue_family_index(new.get_family().get_family_index());
                }
                (Some(old), None) => {
                    builder = builder
                        .src_queue_family_index(old.get_family().get_family_index())
                        .dst_queue_family_index(ash::vk::QUEUE_FAMILY_IGNORED);
                }
                (None, None) => {
                    builder = builder
                        .src_queue_family_index(ash::vk::QUEUE_FAMILY_IGNORED)
                        .dst_queue_family_index(ash::vk::QUEUE_FAMILY_IGNORED);
                }
            }
        } else {
            builder = builder
                .src_queue_family_index(ash::vk::QUEUE_FAMILY_IGNORED)
                .dst_queue_family_index(ash::vk::QUEUE_FAMILY_IGNORED);
        }

        //Now setup the offset buffer and size info
        if let Some(off) = offset {
            builder = builder.offset(off);
        } else {
            builder = builder.offset(0);
        }

        if let Some(sz) = size {
            builder = builder.size(sz);
        } else {
            builder = builder.size(ash::vk::WHOLE_SIZE);
        }

        //always the whole buffer
        builder = builder.buffer(self.buffer);

        builder.build()
    }
}

impl Drop for Buffer {
    fn drop(&mut self) {
        //delocate the memory and delete the buffer.
        self.device
            .get_allocator_locked()
            .expect("Failed to lock allocator while dropping buffer!")
            .destroy_buffer(self.device.vko(), self.buffer, &self.memory_handle);
    }
}

///A trait which provides several different upload strategies to manipulate buffers
/// and images
pub trait UploadStrategy {
    /// Uploads data to this buffer, by directly mapping to it. Only works if `HOST_VISIBLE`.
    ///
    /// You can also use `update_buffer` which uses vulkan internal update function. While `map_data` might be faster for `HOST_VISIBLE` memory, the update function
    /// could be faster for small `DEVICE_LOCAL` memory. You'll have to try that out.
    fn map_data<T: Copy + Sized>(&self, data: Vec<T>) -> Result<(), AllocationError>;
    ///Adds a copy command to the command buffer which will copy `data` from a staging buffer
    /// to `self`.
    /// Should be used when:
    ///
    /// - `self` is `DEVICE_LOCAL`
    /// - The upload has to be synchronized with other commands
    /// - Many buffers have to be uploaded at once.
    ///
    /// #Safety
    /// 1. Assumes that the command buffer is in the recording state, but not within a rendepass.
    ///
    /// 2. Returns the buffer with the TRANSFER_WRITE flag set.
    ///
    /// 3. Make sure the transfer_dst usage is set!
    fn copy_sync<T: Copy + Sized>(
        &self,
        command_buffer: Arc<CommandBuffer>,
        data: Vec<T>,
    ) -> Result<(), AllocationError>;

    ///Creates a new command buffer which will be executed to upload the data from a staging,
    /// host local buffer to `self`
    /// Should be used when:
    ///
    /// - synchronization is not needed
    /// - time constrains are not that strict
    ///
    /// returns a fence which represents the moment uploading has finished. Returns the buffer with the TRANSFER_WRITE access flag set.
    /// #Safety
    /// Make sure that the transfer_dst usage is enabled, otherwise copying won't work!
    fn copy<T: Copy + Sized>(
        &self,
        upload_queue: Arc<Queue>,
        data: Vec<T>,
    ) -> Result<QueueFence, AllocationError>;
}

impl UploadStrategy for Arc<Buffer> {
    fn map_data<T: Copy + Sized>(&self, data: Vec<T>) -> Result<(), AllocationError> {
        //Debug check if there is enough room for "data"
        debug_assert!(
            self.size >= (data.len() * std::mem::size_of::<T>()) as u64,
            "There is not enough room in this buffer for \"data\" supplied: size_of(buffer)={} <=  size_of(data)={}",
            self.size,
            (data.len() * std::mem::size_of::<T>()) as u64,
        );

        if let Some(mut mapped_ptr) = self.memory_handle.mapped_ptr() {
            let mut data_slice = unsafe {
                ash::util::Align::new(
                    mapped_ptr.as_mut(),
                    std::mem::align_of::<T>() as u64,
                    self.memory_handle.size(),
                )
            };
            //Copy, then drop the ptr to unmap
            data_slice.copy_from_slice(&data);

            Ok(())
        } else {
            Err(AllocationError::MapFailed)
        }
    }

    fn copy_sync<T: Copy + Sized>(
        &self,
        command_buffer: Arc<CommandBuffer>,
        data: Vec<T>,
    ) -> Result<(), AllocationError> {
        let data_size = (data.len() * std::mem::size_of::<T>()) as u64;
        //Debug check if there is enough room for "data"
        debug_assert!(
            self.size >= (data.len() * std::mem::size_of::<T>()) as u64,
            "There is not enough room in this buffer for \"data\" supplied: size_of(buffer)={} <=  size_of(data)={}",
            self.size,
            (data.len() * std::mem::size_of::<T>()) as u64,
        );

        //Create ram memory, must be host coherent
        let (staging_fence, staging_buffer) = Buffer::from_data(
            self.device.clone(),
            command_buffer.get_queue(),
            data,
            BufferUsage {
                transfer_src: true,
                ..Default::default()
            },
            SharingMode::Exclusive,
            MemoryUsage::CpuToGpu,
        )?;
        staging_fence
            .wait(u64::MAX)
            .expect("Failed to wait for staging buffers fence!");

        command_buffer
            .pipeline_barrier(
                PipelineBarrier::<1>::new(
                    PipelineStageFlags::BOTTOM_OF_PIPE,
                    PipelineStageFlags::TRANSFER,
                )
                .with_buffer_barrier(
                    BufferMemoryBarrierBuilder::new(self.clone())
                        .with_access_mask(AccessMaskEvent::Acquire(AccessFlags::TRANSFER_WRITE)),
                )
                .build(),
            )
            .unwrap();

        //copy to device memory
        command_buffer
            .cmd_copy_buffer(
                staging_buffer,
                self.clone(),
                vec![ash::vk::BufferCopy {
                    src_offset: 0,
                    dst_offset: 0,
                    size: data_size, //Only copy the data
                }],
            )
            .expect("Could not add copy command for buffer!");

        Ok(())
    }

    fn copy<T: Copy + Sized>(
        &self,
        upload_queue: Arc<Queue>,
        data: Vec<T>,
    ) -> Result<QueueFence, AllocationError> {
        let data_size = (data.len() * std::mem::size_of::<T>()) as u64;
        //Debug check if there is enough room for "data"
        debug_assert!(
            self.size >= data_size as u64,
            "There is not enough room in this buffer for \"data\" supplied: size_of(buffer)={} <=  size_of(data)={}",
            self.size,
            (data.len() * std::mem::size_of::<T>()) as u64,
        );

        //Start command buffer
        let command_pool = match CommandBufferPool::new(
            self.device.clone(),
            upload_queue.clone(),
            ash::vk::CommandPoolCreateFlags::empty(),
        ) {
            Ok(cbp) => cbp,
            Err(er) => return Err(AllocationError::UploadFailed(er)),
        };

        let cb: Arc<CommandBuffer> = match command_pool.alloc(1, false) {
            Ok(cbs) => cbs[0].clone(),
            Err(er) => return Err(AllocationError::UploadFailed(er)),
        };

        let _ = cb
            .begin_recording(true, false, false, None)
            .expect("Could not start upload command buffer");
        //Create ram memory, must be host coherent
        let (staging_fence, staging_buffer) = Buffer::from_data(
            self.device.clone(),
            upload_queue.clone(),
            data,
            BufferUsage {
                transfer_src: true,
                ..Default::default()
            },
            SharingMode::Exclusive,
            MemoryUsage::CpuToGpu,
        )?;
        staging_fence
            .wait(u64::MAX)
            .expect("Failed to wait for staging buffers fence!");

        cb.pipeline_barrier(
            PipelineBarrier::<1>::new(
                PipelineStageFlags::BOTTOM_OF_PIPE,
                PipelineStageFlags::TRANSFER,
            )
            .with_buffer_barrier(
                BufferMemoryBarrierBuilder::new(self.clone())
                    .with_access_mask(AccessMaskEvent::Acquire(AccessFlags::TRANSFER_WRITE)),
            )
            .build(),
        )
        .unwrap();

        //copy to device memory
        cb.cmd_copy_buffer(
            staging_buffer,
            self.clone(),
            vec![ash::vk::BufferCopy {
                src_offset: 0,
                dst_offset: 0,
                size: data_size, //only copy the size of data, the allocator apparently might allocate slightly too much (alignment etc.)
            }],
        )
        .expect("Could not add copy command for buffer!");

        cb.end_recording()
            .expect("Could not end copy command buffer");
        //We have to create a staging buffer, and a command buffer, then upload the data and then wait for it to be finished.
        let submit_fence = upload_queue
            .queue_submit(vec![SubmitInfo::new(vec![], vec![cb], vec![])])
            .expect("Failed to Submit Command buffer!");

        Ok(submit_fence)
    }
}
