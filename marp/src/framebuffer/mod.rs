/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

//! # Framebuffer vs Renderpass
//! The concept of a Renderpass and a Framebuffer are quiet connected. The Renderpass describes in which order
//! something is drawn to which attached images (also called input attachments). So it describes how and when
//! data is written or read from the attached images. The Framebuffer on the other hand describes which images are used
//! for those operations. It is therefore important to actually use the same amount of images and and their order
//! as specified in the Renderpass.
//!
//! # Framebuffer
//! As described above the Framebuffer describes which images should be used for an Renderpass. Since Renderpasses usually don't
//! change so much while a application is running it makes sense to build the renderpass as well as the frambuffer(s) on the startup of the
//! application.
//! ## Usage
//!
//! ## Best practice
//!
//! # Renderpass
//! ## What does it do?
//! A renderpass describes in which stages a frame (or a part of it) is recorded. For instance first render to 4 target images (a G-Buffer)
//! then we take two of those images to render to a new one (SSAO pass maybe).
//! Since vulkan does not know in which order it has to process this it also provide dependencies between those stages (also called subpasses) as well as the
//! attachments that are actually used per subpass.
//!
//! # Usage
//! With the `RenderPassBuilder` you can configure a renderpass and its subpasses.
//! When recording a command-buffer you can start a renderpass via `commandbuffer.begin_renderpass()` and end it in the same way.
//! Between those calls you can record commands to the currently active subpass as well as change to the next subpass via `commandbuffer.next_subpass()`.
//!
//! # Security
//! While creating the AttachmentsDescription for a Renderpass be sure that you also comply with the general rules
//! described in the vulkan spec. Most of them are not checked on runtime.
//!
//!
//!

pub mod renderpass;
pub use renderpass::{
    AttachmentDescription, AttachmentRole, RenderPass, RenderPassBuilder, Subpass,
    SubpassDependency,
};

use crate::device::Device;
use crate::image::AbstractImage;
use std::sync::Arc;

#[derive(Debug)]
pub enum FramebufferError {
    NoAttachments,
    VkResult(ash::vk::Result),
}
pub struct Framebuffer {
    #[allow(dead_code)]
    attachments: Vec<Arc<dyn AbstractImage + Send + Sync>>,
    device: Arc<Device>,
    #[allow(dead_code)]
    renderpass: Arc<RenderPass>,
    framebuffer: ash::vk::Framebuffer,
}

impl Framebuffer {
    pub fn new(
        device: Arc<Device>,
        renderpass: Arc<RenderPass>,
        attachments: Vec<Arc<dyn AbstractImage + Send + Sync>>,
    ) -> Result<Arc<Self>, FramebufferError> {
        if attachments.len() < 1 {
            return Err(FramebufferError::NoAttachments);
        }

        //First of all, check that all images have the same height, and width and use the same layer(?)
        let mut width: Option<u32> = None;
        let mut height: Option<u32> = None;

        for at in attachments.iter() {
            let extent = at.extent();
            if let Some(wd) = width {
                //Only debug check that the width is okay
                debug_assert!(
                    wd == extent.width,
                    "The width of all images in a framebuffer should be equal!"
                );
            } else {
                width = Some(extent.width);
            }

            if let Some(ht) = height {
                //Only debug check that the width is okay
                debug_assert!(
                    ht == extent.height,
                    "The height of all images in a framebuffer should be equal!"
                );
            } else {
                height = Some(extent.height);
            }
        }

        debug_assert!(width.is_some(), "Could not find width for frambuffer");
        debug_assert!(height.is_some(), "Could not find height for frambuffer");
        //Now get us a collection of the image views we need
        let image_views: Vec<ash::vk::ImageView> = attachments
            .iter()
            .map(|img| *img.as_vk_image_view())
            .collect();

        let info = ash::vk::FramebufferCreateInfo::builder()
            .render_pass(*renderpass.vko())
            .attachments(&image_views)
            .width(width.expect("Could not pass width to frambuffer"))
            .height(height.expect("Could not pass height to framebuffer"))
            .layers(1);

        let framebuffer = unsafe {
            match device.vko().create_framebuffer(&info, None) {
                Ok(fb) => fb,
                Err(er) => {
                    return Err(FramebufferError::VkResult(er));
                }
            }
        };

        Ok(Arc::new(Framebuffer {
            attachments,
            device,
            renderpass,
            framebuffer,
        }))
    }

    pub fn vko(&self) -> &ash::vk::Framebuffer {
        &self.framebuffer
    }
}

impl Drop for Framebuffer {
    fn drop(&mut self) {
        unsafe {
            self.device
                .vko()
                .destroy_framebuffer(self.framebuffer, None);
        }
    }
}
