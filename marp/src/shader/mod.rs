/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

//! # Shader
//! This module makes compiling shader as well as verifying shader inputs easy.
//!
//! # Shader, shader-modules and shader-stages
//! A shader is a small program, designed to run on a GPU. However, modern GPUs have several stages that,
//! at some point output the pixels for your screen.
//! Each stage is responsible for different transformation of input data to output data.
//! To represent this, the Vulkan API has three concepts:
//! - Shader: Is the shader code, compiled into a SPIR-V file
//! - Shader-Module: Is the SPIR-V file, loaded into a "Module" object
//! - Shader-Stage: Contains such a Shader-Module, but also knows for which shader-stage (vertex,fragment,compute, etc.) the shader was designed for
//! # Usage in Marp
//! In Marp-Vk you'll have only two of those. The ShaderModule containing a loaded file as well as the module, and the Shader-Stage.
//! The ShaderStage can be generated from the ShaderModule (using the `AbstractShaderModule` implementation) with different attributes. For instance you can define which specializations should be used when creating the Stage
//! which makes it easy to create permutations of a single shader quiet fast at runtime.
//! You can also specify which function ("entry_point") should be used as "main" when starting the shader on the gpu.
//!

use crate::device::*;
use ash;
use std;

use std::sync::Arc;

#[derive(Debug)]
pub enum ShaderStageError {
    FileOpeningError(std::io::Error),
    SprvReadingError(std::io::Error),
    ModuleCreateError(ash::vk::Result),
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum Stage {
    Vertex,
    TessellationControl,
    TessellationEvaluation,
    Geometry,
    Fragment,
    Compute,
}

impl Stage {
    pub fn to_shader_stage_flag(&self) -> ash::vk::ShaderStageFlags {
        match self {
            Stage::Vertex => ash::vk::ShaderStageFlags::VERTEX,
            Stage::TessellationControl => ash::vk::ShaderStageFlags::TESSELLATION_CONTROL,
            Stage::TessellationEvaluation => ash::vk::ShaderStageFlags::TESSELLATION_EVALUATION,
            Stage::Geometry => ash::vk::ShaderStageFlags::GEOMETRY,
            Stage::Fragment => ash::vk::ShaderStageFlags::FRAGMENT,
            Stage::Compute => ash::vk::ShaderStageFlags::COMPUTE,
        }
    }
}

//TODO Research specialisazion Constant/Info

pub struct ShaderModule {
    shader_module: ash::vk::ShaderModule,
    code: Vec<u32>,
    device: Arc<Device>,
}

impl ShaderModule {
    ///Creates a new shader stage from a already compiled shader
    pub fn new_from_spv<P: AsRef<std::path::Path> + Copy>(
        device: Arc<Device>,
        file_path: P,
    ) -> Result<Arc<Self>, ShaderStageError> {
        let mut file = match std::fs::File::open(file_path) {
            Ok(f) => f,
            Err(er) => return Err(ShaderStageError::FileOpeningError(er)),
        };

        let code = match ash::util::read_spv(&mut file) {
            Ok(c) => c,
            Err(er) => return Err(ShaderStageError::SprvReadingError(er)),
        };

        let shader_mod_info = ash::vk::ShaderModuleCreateInfo::builder().code(&code);
        let shader_module = unsafe {
            match device.vko().create_shader_module(&shader_mod_info, None) {
                Ok(module) => module,
                Err(er) => return Err(ShaderStageError::ModuleCreateError(er)),
            }
        };

        Ok(Arc::new(ShaderModule {
            code,
            shader_module: shader_module,
            device,
        }))
    }
    ///Creates the shader module from a raw slice of spir-v bytes.
    /// An easy way to store your shader in the binary would be a function like this:
    ///```Rust
    ///fn def_frag_code() -> Vec<u32>{
    ///    let bytes = include_bytes!("../../examples/shader/cube_textures/frag.spv");
    ///    ash::util::read_spv(&mut std::io::Cursor::new(&bytes[..])).expect("fail")
    ///}
    ///```
    pub fn new_from_code(
        device: Arc<Device>,
        code: Vec<u32>,
    ) -> Result<Arc<Self>, ShaderStageError> {
        let code_copy = code;
        let shader_mod_info = ash::vk::ShaderModuleCreateInfo::builder().code(&code_copy);
        let shader_module = unsafe {
            match device.vko().create_shader_module(&shader_mod_info, None) {
                Ok(module) => module,
                Err(er) => return Err(ShaderStageError::ModuleCreateError(er)),
            }
        };

        Ok(Arc::new(ShaderModule {
            code: code_copy,
            shader_module: shader_module,
            device,
        }))
    }

    pub fn vko(&self) -> ash::vk::ShaderModule {
        self.shader_module.clone()
    }

    ///Returns the bytecode used for this shader.
    pub fn code(&self) -> &Vec<u32> {
        &self.code
    }
}

impl Drop for ShaderModule {
    fn drop(&mut self) {
        unsafe {
            self.device
                .vko()
                .destroy_shader_module(self.shader_module, None);
        }
    }
}

pub trait AbstractShaderModule {
    fn to_stage(&self, stage: Stage, entry_name: &str) -> Arc<ShaderStage>;
}

impl AbstractShaderModule for Arc<ShaderModule> {
    fn to_stage(&self, stage: Stage, entry_name: &str) -> Arc<ShaderStage> {
        let name_string = std::ffi::CString::new(entry_name)
            .expect("Could not convert entry function string to CString");

        Arc::new(ShaderStage {
            module: self.clone(),
            stage,
            entry_name: name_string,
        })
    }
}

pub struct ShaderStage {
    module: Arc<ShaderModule>,
    stage: Stage,
    entry_name: std::ffi::CString,
}

impl ShaderStage {
    pub fn vko<'a>(
        &'a self,
        specialization_info: Option<&'a ash::vk::SpecializationInfo>,
    ) -> ash::vk::PipelineShaderStageCreateInfoBuilder<'a> {
        let mut stage_info = ash::vk::PipelineShaderStageCreateInfo::builder()
            .stage(self.stage.to_shader_stage_flag())
            .module(self.module.vko())
            .name(&self.entry_name);

        if let Some(special) = specialization_info {
            stage_info = stage_info.specialization_info(special);
        }

        stage_info
    }

    pub fn stage(&self) -> Stage {
        self.stage
    }
}
