/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

use std::default::Default;
use std::ffi::{CStr, CString};
use std::os::raw::c_void;
use std::ptr;

use ash::*;
use ash::{
    extensions::*,
    vk::{
        DebugUtilsMessageSeverityFlagsEXT, DebugUtilsMessageTypeFlagsEXT,
        DebugUtilsMessengerCallbackDataEXT,
    },
};

///Used to mark all extensions a instance can support.
#[derive(Debug)]
pub struct InstanceExtensions {
    pub android_surface: bool,
    pub debug_utils: bool,
    pub display_swapchain: bool,
    pub ios_surface: bool,
    pub macos_surface: bool,
    pub surface: bool,
    ///Should usually be activated if you want to present something with vulkan along side a surface.
    pub swapchain: bool,
    pub wayland_surface: bool,
    pub win32_surface: bool,
    pub xcb_surface: bool,
    pub xlib_surface: bool,
}

impl Default for InstanceExtensions {
    ///Initializes everything to false.
    fn default() -> Self {
        InstanceExtensions {
            android_surface: false,
            debug_utils: false,
            display_swapchain: false,
            ios_surface: false,
            macos_surface: false,
            surface: false,
            swapchain: false,
            wayland_surface: false,
            win32_surface: false,
            xcb_surface: false,
            xlib_surface: false,
        }
    }
}

impl InstanceExtensions {
    ///Shortcut to activate all extension needed to present something on either
    /// Linux or Windows
    #[cfg(all(windows))]
    pub fn presentable() -> InstanceExtensions {
        #[cfg(feature = "logging")]
        log::info!("Adding presenting surfaces for Windows!");
        InstanceExtensions {
            surface: true,
            win32_surface: true,
            ..Default::default()
        }
    }
    /// Layers for linux
    /// *Warning* This will avtivate the layers for the xwindow system (xlib_surface).
    /// If you want to use the wayland_surface instead, you'll have to manipulate it yourself.
    #[cfg(all(unix, not(target_os = "android")))]
    pub fn presentable() -> InstanceExtensions {
        #[cfg(feature = "logging")]
        log::info!("DEBUG: Adding presenting surfaces for Linux!");
        InstanceExtensions {
            surface: true,
            xlib_surface: true,
            wayland_surface: true, //currently always adding wayland surface since this is a shortcut anyways.
            ..Default::default()
        }
    }

    ///Converts the list to actual values.
    pub fn to_names(&self) -> Vec<*const i8> {
        let mut all = Vec::new();

        if self.android_surface {
            all.push(khr::AndroidSurface::name().as_ptr());
        }
        if self.debug_utils {
            all.push(ext::DebugUtils::name().as_ptr());
        }
        if self.display_swapchain {
            all.push(khr::DisplaySwapchain::name().as_ptr());
        }
        if self.ios_surface {
            all.push(mvk::IOSSurface::name().as_ptr());
        }
        if self.macos_surface {
            all.push(mvk::MacOSSurface::name().as_ptr());
        }
        if self.surface {
            all.push(khr::Surface::name().as_ptr());
        }
        if self.swapchain {
            all.push(khr::Swapchain::name().as_ptr());
        }
        if self.wayland_surface {
            all.push(khr::WaylandSurface::name().as_ptr());
        }
        if self.win32_surface {
            all.push(khr::Win32Surface::name().as_ptr());
        }
        if self.xcb_surface {
            all.push(khr::XcbSurface::name().as_ptr());
        }
        if self.xlib_surface {
            all.push(khr::XlibSurface::name().as_ptr());
        }
        all
    }
}

///Represents a device extension. You'll have to activate a swapchain here as well if you want to draw to a surfaceKHR.
///I decided to wrap this type since working with CStrings is not nice.
#[derive(Debug, Clone)]
pub struct DeviceExtension {
    name: String,
    c_name: CString,
    min_version: u32,
}

impl DeviceExtension {
    pub fn new(name: String, min_version: u32) -> Self {
        let cast: Vec<u8> = name.clone().into();
        let self_cstr = CString::new(cast).expect("Failed to build CString from string");

        DeviceExtension {
            name,
            c_name: self_cstr,
            min_version,
        }
    }
    ///Returns true if this DeviceExtension is within a list of `DeviceExtension`s, and it fullfills at least the min_version.
    pub fn contained(&self, other_extensions: &[DeviceExtension]) -> bool {
        for i in other_extensions {
            if i.get_name() == self.get_name() && i.get_version() >= self.get_version() {
                return true;
            }
        }

        false
    }

    pub fn get_name(&self) -> &str {
        &self.name
    }

    pub fn get_version(&self) -> u32 {
        self.min_version
    }

    pub fn as_c_str(&self) -> &CStr {
        self.c_name.as_c_str()
    }
}

///An instance layer that can add additional functionality to vulkan.
pub struct InstanceLayer {
    layer_name: CString,
}

impl InstanceLayer {
    pub fn name(&self) -> &CStr {
        &self.layer_name
    }

    //Creates a new Instance layer form the given name
    pub fn new(layer_name: &str) -> Result<Self, std::ffi::NulError> {
        let c_str: Vec<u8> = layer_name.into();
        Ok(InstanceLayer {
            layer_name: CString::new(c_str)?,
        })
    }
    ///Creates a vector of commonly used debug layers. (Currently only lunarg layer)
    ///# Safety
    /// Make sure the following layers are installed, otherwise instance creation will fail.
    ///
    /// - LunarG standard validation
    pub fn debug_layers() -> Vec<Self> {
        vec![InstanceLayer {
            layer_name: CString::new("VK_LAYER_KHRONOS_validation")
                .expect("failed to create CString Lunar g"),
        }]
    }

    pub fn as_ptr(&self) -> *const i8 {
        self.layer_name.as_ptr()
    }
}

///Simple wrapper to store a vulkan version of something
#[derive(Copy, Clone)]
pub struct Version {
    pub major: usize,
    pub minor: usize,
    pub patch: usize,
}

impl Version {
    pub fn new(major: usize, minor: usize, patch: usize) -> Self {
        Version {
            major,
            minor,
            patch,
        }
    }

    pub fn to_version(&self) -> u32 {
        (((self.major) << 22) | ((self.minor) << 12) | (self.patch)) as u32
    }
}

///Hold Application info which can be referenced by drivers for instance.
/// NOTE: You can't specify the vulkan version here. To do that, set the `VkVersion` type in the crates root to something else and recompile.
pub struct AppInfo {
    c_name: CString,
    ///Major, Minor and patch value in that order
    app_version: Version,
    c_engine_name: CString,
    engine_version: Version,
    ///The api version that should be used
    api_version: Version,
    //Holds the actual data for vulkan,
    vk_data: vk::ApplicationInfo,
}

///Generates an empty info to be replaced
fn tmp_app_info() -> vk::ApplicationInfo {
    vk::ApplicationInfo {
        p_application_name: ptr::null(),
        s_type: vk::StructureType::APPLICATION_INFO,
        p_next: ptr::null(),
        application_version: 0,
        p_engine_name: ptr::null(),
        engine_version: 0,
        api_version: Version::new(1, 0, 89).to_version(),
    }
}
#[allow(unused_variables)]
impl AppInfo {
    pub fn new(
        name: String,
        app_version: Version,
        engine_name: String,
        engine_version: Version,
        api_version: Version,
    ) -> Self {
        let raw_name: Vec<u8> = name.into();
        let raw_engine_name: Vec<u8> = engine_name.clone().into();

        let mut info = AppInfo {
            c_name: CString::new(raw_name).expect("Failed to get C name"),
            app_version,
            c_engine_name: CString::new(raw_engine_name).expect("failed to get C engine name"),
            engine_version,
            api_version,
            vk_data: tmp_app_info(),
        };
        info.update_vk_data();
        info
    }
    ///Creates a default set where you specify the name, the api version is set to 1.0.89 and the engine/app version is 0.1.0. The engine name is set to "Default"
    pub fn default(name: String) -> Self {
        let raw_name: Vec<u8> = name.into();
        let mut info = AppInfo {
            c_name: CString::new(raw_name).expect("Failed to get C name"),
            app_version: Version::new(0, 1, 0),
            c_engine_name: CString::new("Default").expect("failed to get C engine name"),
            engine_version: Version::new(0, 1, 0),
            api_version: Version::new(1, 1, 0),
            vk_data: tmp_app_info(),
        };
        info.update_vk_data();
        info
    }

    pub fn get_api_version(&self) -> &Version {
        &self.api_version
    }

    pub fn set_api_version(&mut self, ver: Version) {
        self.api_version = ver;
    }

    fn update_vk_data(&mut self) {
        //Convert the name to a raw *const i8
        self.vk_data = vk::ApplicationInfo {
            p_application_name: self.c_name.as_ptr(),
            s_type: vk::StructureType::APPLICATION_INFO,
            p_next: ptr::null(),
            application_version: self.app_version.to_version(),
            p_engine_name: self.c_engine_name.as_ptr(),
            engine_version: self.engine_version.to_version(),
            api_version: self.api_version.to_version(),
        };

        /*
                self.vk_data = vk::ApplicationInfo::builder()
                    .application_name(&self.c_name)
                    .application_version(self.app_version.to_version())
                    .engine_name(&self.c_engine_name)
                    .engine_version(self.engine_version.to_version())
                    .api_version(self.app_version.to_version())
                    .build()
        */
    }

    pub fn to_vk_app_info(&self) -> &vk::ApplicationInfo {
        &self.vk_data
    }
}

///The external callback prinf function for debugging
///TODO change depending on warning etc.
unsafe extern "system" fn vulkan_debug_callback(
    message_severity: DebugUtilsMessageSeverityFlagsEXT,
    message_types: DebugUtilsMessageTypeFlagsEXT,
    p_callback_data: *const DebugUtilsMessengerCallbackDataEXT,
    _p_user_data: *mut c_void,
) -> u32 {
    if p_callback_data == ptr::null() {
        #[cfg(feature = "logging")]
        log::error!("MarpDebugMsg: Got Msg, but no data!");
        return 1;
    }

    println!(
        "MarpDebugMsg: Level: {:?}, Type: {:?}\n",
        message_severity, message_types
    );
    //If there is a validation layer msg, append
    if !(*p_callback_data).p_message_id_name.is_null() {
        println!(
            "Id[{:?}]: {:?}",
            (*p_callback_data).message_id_number,
            CStr::from_ptr((*p_callback_data).p_message_id_name)
        )
    } else {
        println!("Id[{:?}]", (*p_callback_data).message_id_number);
    }

    //Now display message if there is any
    if !(*p_callback_data).p_message.is_null() {
        println!("Msg: {:?}", CStr::from_ptr((*p_callback_data).p_message));
    }

    1
}

///Defines how, at instance creation the debugging (layers) should be handeled
pub struct Debugging {
    pub should_debug: bool,
    pub on_info: bool,
    pub on_warning: bool,
    pub on_error: bool,
    ///If true the callback will also display Performance warnings
    pub display_performance_warning: bool,
    ///If true warnings will be verbose
    pub display_verbose: bool,
}

///Initializes everything to false
impl Default for Debugging {
    fn default() -> Debugging {
        Debugging {
            should_debug: false,
            on_info: false,
            on_warning: false,
            on_error: false,
            display_performance_warning: false,
            display_verbose: false,
        }
    }
}

impl Debugging {
    //Reports everything
    pub fn all() -> Self {
        Debugging {
            should_debug: true,
            on_info: true,
            on_warning: true,
            on_error: true,
            display_performance_warning: true,
            display_verbose: true,
        }
    }

    pub fn errors() -> Self {
        Debugging {
            should_debug: true,
            on_info: false,
            on_warning: true,
            on_error: true,
            display_performance_warning: false,
            display_verbose: false,
        }
    }

    pub fn none() -> Self {
        Debugging {
            should_debug: false,
            on_info: false,
            on_warning: false,
            on_error: false,
            display_performance_warning: false,
            display_verbose: false,
        }
    }

    pub fn to_info(&self) -> vk::DebugUtilsMessengerCreateInfoEXT {
        let mut flag = vk::DebugUtilsMessageSeverityFlagsEXT::empty();
        if self.should_debug {
            if self.on_info {
                flag = flag | vk::DebugUtilsMessageSeverityFlagsEXT::INFO;
            }

            if self.on_warning {
                flag = flag | vk::DebugUtilsMessageSeverityFlagsEXT::WARNING;
            }

            if self.on_error {
                flag = flag | vk::DebugUtilsMessageSeverityFlagsEXT::ERROR;
            }

            if self.display_verbose {
                flag = flag | vk::DebugUtilsMessageSeverityFlagsEXT::VERBOSE;
            }
        }

        let mut level_flag = vk::DebugUtilsMessageTypeFlagsEXT::empty();
        if self.should_debug {
            level_flag = level_flag | vk::DebugUtilsMessageTypeFlagsEXT::VALIDATION;
            level_flag = level_flag | vk::DebugUtilsMessageTypeFlagsEXT::GENERAL;

            if self.display_performance_warning {
                level_flag = level_flag | vk::DebugUtilsMessageTypeFlagsEXT::PERFORMANCE;
            }
        }

        vk::DebugUtilsMessengerCreateInfoEXT::builder()
            .message_severity(flag)
            .message_type(level_flag)
            .pfn_user_callback(Some(vulkan_debug_callback))
            .build()

        /*{
            s_type: vk::StructureType::DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT,
            p_next: ptr::null(),
            flags: flag,
            pfn_callback: Some(vulkan_debug_callback),
            p_user_data: ptr::null_mut(),
        }*/
    }
}
