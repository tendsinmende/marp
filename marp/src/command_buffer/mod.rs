/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

//! # Job of the CommandBuffer
//! The command buffer records, well the commands you want to sent to the GPU.
//! You use a command buffer every time you want to interact with the GPU, this includes drawing
//! a vertex buffer as well as uploading a buffer or memory to the GPU from the Host (CPU) side.
//!
//! # CommandBufferPools
//! Since command buffers are used so often there exist CommandBufferPools. They only allocate enough space for n command
//! buffers. You can fill one and flush it to the device. While the first one is processing you can already start to record a second one.
//!
//! # Concurrent recording.
//! The CommandBufferPool as well as the recording commands should not be used concurrently.
//! However the marp wrapper around those objects uses CPU synchronization (via `Mutex` locks) to ensure
//! that only one thread uses a CommandBuffer or CommandBufferPool at once. Also each CommandBuffer allocated form a pool is tracked by the
//! Pool and destroyed if not referenced anymore.
//!
//! # CommandPool and queues
//! Every command pool is build for a special queue_family. When submitting a
//! command to a CommandBuffer created from a pool, be sure that the queue family index is the same for the queue used
//! for creation as the queue you want to execute the command on.
//!
//! # Debug marker
//! It is possible to set debug marker within a command buffer. Either for marking regions, or for naming objects.
//! With in a command buffer you can call `debug_start_region()`, `debug_end_region()` and `debug_marker_insert()`.
//! However, the action will only be done if the debug layer is present, otherwise calling those functions won't have an effect
//! other than printing a Warning to stdout.

mod pipeline_barrier;
pub use pipeline_barrier::{
    AccessMaskEvent, BufferMemoryBarrierBuilder, ImageLayoutTransitionEvent,
    ImageMemoryBarrierBuilder, MemoryBarrierBuilder, PipelineBarrier, PipelineBarrierBuilder,
    QueueEvent,
};

use ash::vk;
use ash::{self, vk::DebugUtilsLabelEXT};
use std::result::Result;
use std::{
    ffi::CStr,
    sync::{Arc, Mutex, MutexGuard, RwLock},
};

use crate::buffer::Buffer;
use crate::descriptor::{DescriptorSet, PushConstant};
use crate::device::{Device, Queue};
use crate::framebuffer::{Framebuffer, RenderPass};
use crate::image::AbstractImage;
use crate::pipeline::{Pipeline, PipelineLayout};

pub trait CommandPool {
    ///Allocates n-times a new command buffer.
    fn alloc(
        &self,
        count: usize,
        is_secondary: bool,
    ) -> Result<Vec<Arc<CommandBuffer>>, ash::vk::Result>;
    fn trim(&self);
    ///Resets all command buffers belonging to this pool.
    fn reset(&self, flags: ash::vk::CommandPoolResetFlags) -> Result<(), ash::vk::Result>;
}

///The standard CommandBufferPools. Keep in mind that this pool and its sub buffer should not be used simultaneously in different threads.
pub struct CommandBufferPool {
    pool: Mutex<ash::vk::CommandPool>,
    //The queue we can submit commands for
    queue: Arc<Queue>,
    device: Arc<Device>,
    flags: ash::vk::CommandPoolCreateFlags,
}

impl CommandBufferPool {
    pub fn new(
        device: Arc<Device>,
        queue: Arc<Queue>,
        command_pool_flags: ash::vk::CommandPoolCreateFlags,
    ) -> Result<Arc<Self>, ash::vk::Result> {
        let pool_info = ash::vk::CommandPoolCreateInfo::builder()
            .flags(command_pool_flags)
            .queue_family_index(queue.get_family().get_family_index());

        let pool = unsafe {
            match device.vko().create_command_pool(&pool_info, None) {
                Ok(res) => res,
                Err(er) => return Err(er),
            }
        };

        Ok(Arc::new(CommandBufferPool {
            pool: Mutex::new(pool),
            queue,
            device,
            flags: command_pool_flags,
        }))
    }

    ///Short for new with RESET_COMMAND_BUFFER flags. Makes this CommandbufferPool's CommandBuffer accept the reset command.
    pub fn new_reset_command_buffer_pool(
        device: Arc<Device>,
        queue: Arc<Queue>,
    ) -> Result<Arc<Self>, ash::vk::Result> {
        CommandBufferPool::new(
            device,
            queue,
            ash::vk::CommandPoolCreateFlags::RESET_COMMAND_BUFFER,
        )
    }

    ///Returns true if this command buffer pool is compatible to the the supplied queue.
    pub fn is_compatible(&self, other_queue: Arc<Queue>) -> bool {
        self.queue.get_family().get_family_index() == other_queue.get_family().get_family_index()
    }

    ///Frees an command buffer allocated from this pool. Might panic if it is from another pool.
    fn free_command_buffer(&self, cb: &CommandBuffer) {
        unsafe {
            self.device.vko().free_command_buffers(
                *self
                    .pool
                    .lock()
                    .expect("Failed to lock pool for cb freeing"),
                &[*cb.vko()],
            );
        }
    }

    fn trim_cbp(&self) {
        unsafe {
            self.device.vko().trim_command_pool(
                *self.pool.lock().expect("failed to trim command pool!"),
                ash::vk::CommandPoolTrimFlags::empty(),
            );
        }
    }

    fn reset_cbp(&self, flags: ash::vk::CommandPoolResetFlags) -> Result<(), ash::vk::Result> {
        unsafe {
            match self.device.vko().reset_command_pool(
                *self.pool.lock().expect("Failed to reset command pool"),
                flags,
            ) {
                Ok(_) => Ok(()),
                Err(er) => Err(er),
            }
        }
    }
}

impl CommandPool for Arc<CommandBufferPool> {
    ///Allocates `count` new command buffers. By default they are all primary command buffers
    /// except you set `is_secondary` to true in which case they are secondary.
    fn alloc(
        &self,
        count: usize,
        is_secondary: bool,
    ) -> Result<Vec<Arc<CommandBuffer>>, ash::vk::Result> {
        let level = if is_secondary {
            ash::vk::CommandBufferLevel::SECONDARY
        } else {
            ash::vk::CommandBufferLevel::PRIMARY
        };

        let alloc_info = ash::vk::CommandBufferAllocateInfo::builder()
            .command_pool(
                *self
                    .pool
                    .lock()
                    .expect("Failed to lock inner command buffer pool"),
            )
            .command_buffer_count(count as u32)
            .level(level);

        let command_buffers = unsafe {
            match self.device.vko().allocate_command_buffers(&alloc_info) {
                Ok(res) => res,
                Err(er) => return Err(er),
            }
        };

        //Now Build our command buffer object for each of them
        let final_command_buffers: Vec<Arc<CommandBuffer>> = command_buffers
            .into_iter()
            .map(|cb| {
                Arc::new(CommandBuffer {
                    device: self.device.clone(),
                    queue: self.queue.clone(),
                    command_buffer: Mutex::new(cb),
                    pool: self.clone(),
                    level,
                    resetable: if self
                        .flags
                        .contains(ash::vk::CommandPoolCreateFlags::RESET_COMMAND_BUFFER)
                    {
                        true
                    } else {
                        false
                    },
                    state: RwLock::new(CommandBufferState::Initial),

                    resources: RwLock::new(Vec::new()),
                    useses_graphics: RwLock::new(false),
                })
            })
            .collect();

        Ok(final_command_buffers)
    }

    fn trim(&self) {
        self.trim_cbp();
    }

    fn reset(&self, flags: ash::vk::CommandPoolResetFlags) -> Result<(), ash::vk::Result> {
        self.reset_cbp(flags)
    }
}

impl Drop for CommandBufferPool {
    fn drop(&mut self) {
        //TODO Check that all child command buffers are NOT in the Pending state.
        unsafe {
            self.device.vko().destroy_command_pool(
                *self
                    .pool
                    .lock()
                    .expect("Failed to lock inner command buffer pool"),
                None,
            );
        }
    }
}

///Describes the current state of the command buffer.
#[derive(PartialEq, Clone, Copy)]
pub enum CommandBufferState {
    Initial,
    Recording,
    Executable,
    Pending,
    Invalid,
}
#[derive(Debug)]
pub enum CommandBufferResetError {
    FailedToReset(ash::vk::Result),
    ///If this command buffer is allocated from a pool created without the `RESET_COMMAND_BUFFER_FLAG`.
    IsNotResetable,
}

#[derive(Debug)]
pub enum CommandBufferTransitionError {
    ///When the command was called on a command buffer in the wrong state.
    WrongState,
    VkError(ash::vk::Result),
}

///Holds resources which have to outlife the command buffer.
pub enum CommandBufferResources {
    Buffer(Arc<Buffer>),
    Image(Arc<dyn AbstractImage + Send + Sync>),
    SecondaryCommandBuffer {
        command_buffer: Arc<CommandBuffer>,
        inner: vk::CommandBuffer,
    },
    DescriptorSet {
        desc_set: Arc<DescriptorSet>,
        inner: vk::DescriptorSet,
    },
    QueryPool(vk::QueryPool),
    RenderPass {
        render_pass: Arc<RenderPass>,
        framebuffer: Arc<Framebuffer>,
        //begin_info: Arc<vk::RenderPassBeginInfo>,
    },
    PipelineLayout(Arc<PipelineLayout>),
    Pipeline(Arc<dyn Pipeline + Send + Sync>),
    //MemoryBarrier(vk::MemoryBarrier),
    //BufferBarrier(vk::BufferMemoryBarrier),
    //ImageBarrier(vk::ImageMemoryBarrier),
    //TODO wrap the barriers so they are sync ?
    DebugLabel(String),
}

/// A CommandBuffer which can be used to record commands.
pub struct CommandBuffer {
    device: Arc<Device>,
    queue: Arc<Queue>,
    command_buffer: Mutex<ash::vk::CommandBuffer>,
    //The pool this cb was allocated from.
    pool: Arc<CommandBufferPool>,

    level: ash::vk::CommandBufferLevel,
    resetable: bool,
    state: RwLock<CommandBufferState>,

    //Captures all resources used in this command buffer
    resources: RwLock<Vec<CommandBufferResources>>,
    //is true when the command must be executed on a graphics capable queue.
    useses_graphics: RwLock<bool>,
}

impl CommandBuffer {
    ///Returns the vulkan command buffer which can be used to record commands. Keep in mind that the command buffer locks
    /// the returned value is dropped at some point.
    pub fn vko<'a>(&'a self) -> MutexGuard<'a, ash::vk::CommandBuffer> {
        let guard = self
            .command_buffer
            .lock()
            .expect("Failed to lock inner command buffer");
        guard
    }

    pub fn is_secondary(&self) -> bool {
        self.level == ash::vk::CommandBufferLevel::SECONDARY
    }

    ///Returns the queue this command buffer was build for
    pub fn get_queue(&self) -> Arc<Queue> {
        self.queue.clone()
    }

    ///Returns true if you can use that queue with that frame buffer (i.e. when the queue family is the same).
    pub fn queue_allowed(&self, other_queue: Arc<Queue>) -> bool {
        self.queue.get_family().get_family_index() == other_queue.get_family().get_family_index()
    }

    pub fn get_device(&self) -> Arc<Device> {
        self.device.clone()
    }

    ///Returns the current state this CommandBuffer is in.
    pub fn get_current_state(&self) -> CommandBufferState {
        *self
            .state
            .read()
            .expect("Failed to read current command buffer state.")
    }

    ///Returns true if this command buffer must be submitted to a graphic capable queue.
    pub fn useses_graphics(&self) -> bool {
        *self
            .useses_graphics
            .read()
            .expect("Could not read cb useses graphics state")
    }

    ///Resets this command buffer and releases all resources bound to the cb.
    pub fn reset(&self) -> Result<(), CommandBufferResetError> {
        if self.resetable {
            unsafe {
                match self.device.vko().reset_command_buffer(
                    *self
                        .command_buffer
                        .lock()
                        .expect("Failed to lock inner command buffer!"),
                    ash::vk::CommandBufferResetFlags::RELEASE_RESOURCES,
                ) {
                    Ok(_) => {
                        //Since we resetted, we can move to the Initial state
                        *self
                            .state
                            .write()
                            .expect("Failed to write to command buffer state") =
                            CommandBufferState::Initial;

                        //Now we delete all caputerd resources
                        self.resources
                            .write()
                            .expect("failed to empty dependency container")
                            .clear();
                        //Reset graphics flag
                        *self
                            .useses_graphics
                            .write()
                            .expect("Could not reset graphics flag") = false;

                        Ok(())
                    }
                    Err(er) => return Err(CommandBufferResetError::FailedToReset(er)),
                }
            }
        } else {
            Err(CommandBufferResetError::IsNotResetable)
        }
    }

    ///Adds an resource needed to be alive for this command buffer to execute.
    fn add_dependent_image(&self, image: Arc<dyn AbstractImage + Send + Sync>) {
        self.resources
            .write()
            .expect("failed to add dep image")
            .push(CommandBufferResources::Image(image));
    }

    fn add_dependent_buffer(&self, buffer: Arc<Buffer>) {
        self.resources
            .write()
            .expect("failed to add dep image")
            .push(CommandBufferResources::Buffer(buffer));
    }

    fn add_dependend_resources(&self, resource: CommandBufferResources) {
        self.resources
            .write()
            .expect("Failed to add dep resouce")
            .push(resource);
    }

    ///Sets the graphics flag for the cb
    fn set_graphics(&self) {
        *self
            .useses_graphics
            .write()
            .expect("Could not reset graphics flag") = true;
    }

    ///Starts the command buffer recording process. Moves the Commandbuffer into the recording state.
    /// In appropriate settings are ignored. For instance the continue bit if it is a primary command buffer.
    pub fn begin_recording<'a>(
        &self,
        one_time_submit: bool,
        renderpass_continue: bool,
        simultaneouse_use: bool,
        inheritance_info: Option<&'a ash::vk::CommandBufferInheritanceInfoBuilder>,
    ) -> Result<(), CommandBufferTransitionError> {
        //Check that we are in the correct state
        if *self.state.read().expect("Failed to read state") != CommandBufferState::Initial {
            return Err(CommandBufferTransitionError::WrongState);
        }
        //Setup the info flags
        let mut flags = ash::vk::CommandBufferUsageFlags::empty();
        if one_time_submit {
            flags = flags | ash::vk::CommandBufferUsageFlags::ONE_TIME_SUBMIT;
        }
        //Only set this if we are a secondary one
        if renderpass_continue && (self.level == ash::vk::CommandBufferLevel::SECONDARY) {
            flags = flags | ash::vk::CommandBufferUsageFlags::ONE_TIME_SUBMIT;
        }
        if simultaneouse_use {
            flags = flags | ash::vk::CommandBufferUsageFlags::SIMULTANEOUS_USE;
        }

        let mut begin_info = ash::vk::CommandBufferBeginInfo::builder().flags(flags);

        let default_info = ash::vk::CommandBufferInheritanceInfo::builder();
        let info = if let Some(t) = inheritance_info {
            t
        } else {
            &default_info
        };
        if self.level == ash::vk::CommandBufferLevel::SECONDARY && inheritance_info.is_some() {
            begin_info = begin_info.inheritance_info(info);
        }
        //Actually transition
        unsafe {
            match self.device.vko().begin_command_buffer(
                *self
                    .command_buffer
                    .lock()
                    .expect("Failed to lock inner command buffer!"),
                &begin_info,
            ) {
                Ok(_) => {}
                Err(er) => return Err(CommandBufferTransitionError::VkError(er)),
            }
        }
        //Handle state transition if it was successful
        *self
            .state
            .write()
            .expect("Failed to transition to recording state") = CommandBufferState::Recording;

        Ok(())
    }

    pub fn end_recording(&self) -> Result<(), CommandBufferTransitionError> {
        if self.get_current_state() != CommandBufferState::Recording {
            return Err(CommandBufferTransitionError::WrongState);
        }

        //We can transition
        unsafe {
            match self.device.vko().end_command_buffer(*self.vko()) {
                Ok(_) => {}
                Err(er) => return Err(CommandBufferTransitionError::VkError(er)),
            }
        }
        //Was well so we transition
        *self
            .state
            .write()
            .expect("Failed to transtion to executable state") = CommandBufferState::Executable;
        Ok(())
    }

    ///Blits one image to the other, might change size etc.
    /// Make sure that the image is in the given layout, before bliting.
    pub fn cmd_blit_image(
        &self,
        src_image: Arc<dyn AbstractImage + Send + Sync>,
        src_image_layout: vk::ImageLayout,
        dst_image: Arc<dyn AbstractImage + Send + Sync>,
        dst_image_layout: vk::ImageLayout,
        regions: Vec<vk::ImageBlit>,
        filter: vk::Filter,
    ) -> Result<(), CommandBufferTransitionError> {
        if self.get_current_state() != CommandBufferState::Recording {
            return Err(CommandBufferTransitionError::WrongState);
        }

        //Execute the command
        unsafe {
            self.device.vko().cmd_blit_image(
                *self.vko(),
                *src_image.as_vk_image(),
                src_image_layout,
                *dst_image.as_vk_image(),
                dst_image_layout,
                &regions,
                filter,
            );
        }
        //We have to store those so they don't go out of scope while being used by the CB.
        self.add_dependent_image(src_image);
        self.add_dependent_image(dst_image);
        self.set_graphics();
        Ok(())
    }

    /// Resolves a multisampled image to a single sampeld image,
    /// make sure the formats and dimensions fit, also check the image layouts to be correct.
    pub fn cmd_resolve_image(
        &self,
        src_image: Arc<dyn AbstractImage + Send + Sync>,
        src_image_layout: vk::ImageLayout,
        dst_image: Arc<dyn AbstractImage + Send + Sync>,
        dst_image_layout: vk::ImageLayout,
        regions: Vec<vk::ImageResolve>,
    ) -> Result<(), CommandBufferTransitionError> {
        if self.get_current_state() != CommandBufferState::Recording {
            return Err(CommandBufferTransitionError::WrongState);
        }

        unsafe {
            self.device.vko().cmd_resolve_image(
                *self.vko(),
                *src_image.as_vk_image(),
                src_image_layout,
                *dst_image.as_vk_image(),
                dst_image_layout,
                regions.as_slice(),
            );
        }

        self.add_dependent_image(src_image);
        self.add_dependent_image(dst_image);
        self.set_graphics();
        Ok(())
    }

    ///Fills a buffer with `data`. Make sure that offset and size are within the buffer range.
    pub fn cmd_fill_buffer(
        &self,
        buffer: Arc<Buffer>,
        offset: u64,
        size: u64,
        data: u32,
    ) -> Result<(), CommandBufferTransitionError> {
        if self.get_current_state() != CommandBufferState::Recording {
            return Err(CommandBufferTransitionError::WrongState);
        }

        unsafe {
            self.device
                .vko()
                .cmd_fill_buffer(*self.vko(), *buffer.vko(), offset, size, data);
        }
        self.add_dependent_buffer(buffer);

        Ok(())
    }

    ///updates a buffer with `data`. Make sure that offset and size of the data are within the buffer range.
    /// #Safety
    /// 1. The buffer must have the TRANSFER_WRITE bit set
    /// 2. The size of data is limited to 65536 byte
    /// 3. Make sure that the data is aligned to the struct you want to read in your shader.
    /// that means every field has to be 16byte aligned and the struct must be marked `repr(C)`
    pub fn cmd_update_buffer<T: Sized>(
        &self,
        buffer: Arc<Buffer>,
        offset: u64,
        data: &[T],
    ) -> Result<(), CommandBufferTransitionError> {
        if self.get_current_state() != CommandBufferState::Recording {
            return Err(CommandBufferTransitionError::WrongState);
        }

        if (std::mem::size_of::<T>() * data.len()) > 65536 {
            #[cfg(feature = "logging")]
            log::warn!("trying to copy too much data via the cmd_update_buffer!");
        }

        //I hope that is the correct way of doing this o.o
        //Get pointer to slice
        let data_pointer: *const T = data.as_ptr();
        //reinterpret as byte pointer
        let byte_pointer: *const u8 = data_pointer as *const u8;
        //build slice pointing to data, with the length of data
        let slice: &[u8] = unsafe {
            std::slice::from_raw_parts(byte_pointer, std::mem::size_of::<T>() * data.len())
        };

        unsafe {
            self.device
                .vko()
                .cmd_update_buffer(*self.vko(), *buffer.vko(), offset, slice);
        }
        self.add_dependent_buffer(buffer);

        Ok(())
    }

    ///Copys one buffer to another. Make sure the offsets as well as sizes fit into both buffer, src and dst.
    pub fn cmd_copy_buffer(
        &self,
        src_buffer: Arc<Buffer>,
        dst_buffer: Arc<Buffer>,
        regions: Vec<vk::BufferCopy>,
    ) -> Result<(), CommandBufferTransitionError> {
        if self.get_current_state() != CommandBufferState::Recording {
            return Err(CommandBufferTransitionError::WrongState);
        }

        unsafe {
            self.device.vko().cmd_copy_buffer(
                *self.vko(),
                *src_buffer.vko(),
                *dst_buffer.vko(),
                &regions,
            );
        }
        self.add_dependent_buffer(src_buffer);
        self.add_dependent_buffer(dst_buffer);
        Ok(())
    }

    ///Copys bits of an image to a buffer. Make sure to transition to the supplied layout when using it.
    pub fn cmd_copy_image_to_buffer(
        &self,
        src_image: Arc<dyn AbstractImage + Send + Sync>,
        src_image_layout: vk::ImageLayout,
        dst_buffer: Arc<Buffer>,
        regions: Vec<vk::BufferImageCopy>,
    ) -> Result<(), CommandBufferTransitionError> {
        if self.get_current_state() != CommandBufferState::Recording {
            return Err(CommandBufferTransitionError::WrongState);
        }

        unsafe {
            self.device.vko().cmd_copy_image_to_buffer(
                *self.vko(),
                *src_image.as_vk_image(),
                src_image_layout,
                *dst_buffer.vko(),
                &regions,
            );
        }
        self.add_dependent_image(src_image);
        self.add_dependent_buffer(dst_buffer);

        Ok(())
    }

    ///Copys bits of an buffer to a image. Make sure to transitiohe supplied layout when using it.
    pub fn cmd_copy_buffer_to_image(
        &self,
        src_buffer: Arc<Buffer>,
        dst_image: Arc<dyn AbstractImage + Send + Sync>,
        dst_image_layout: vk::ImageLayout,
        regions: Vec<vk::BufferImageCopy>,
    ) -> Result<(), CommandBufferTransitionError> {
        if self.get_current_state() != CommandBufferState::Recording {
            return Err(CommandBufferTransitionError::WrongState);
        }

        unsafe {
            self.device.vko().cmd_copy_buffer_to_image(
                *self.vko(),
                *src_buffer.vko(),
                *dst_image.as_vk_image(),
                dst_image_layout,
                &regions,
            );
        }
        self.add_dependent_image(dst_image);
        self.add_dependent_buffer(src_buffer);

        Ok(())
    }

    ///Copies one image to another. Make sur the supplied layouts are correct and teh regions fit into the images.
    pub fn cmd_copy_image(
        &self,
        src_image: Arc<dyn AbstractImage + Send + Sync>,
        src_image_layout: vk::ImageLayout,
        dst_image: Arc<dyn AbstractImage + Send + Sync>,
        dst_image_layout: vk::ImageLayout,
        regions: Vec<vk::ImageCopy>,
    ) -> Result<(), CommandBufferTransitionError> {
        if self.get_current_state() != CommandBufferState::Recording {
            return Err(CommandBufferTransitionError::WrongState);
        }

        unsafe {
            self.device.vko().cmd_copy_image(
                *self.vko(),
                *src_image.as_vk_image(),
                src_image_layout,
                *dst_image.as_vk_image(),
                dst_image_layout,
                regions.as_slice(),
            );
        }

        self.add_dependent_image(src_image);
        self.add_dependent_image(dst_image);

        Ok(())
    }

    ///Binds a index buffer which is used to decide which vertices in a vertex buffer are drawn in which order.
    pub fn cmd_bind_index_buffer(
        &self,
        buffer: Arc<Buffer>,
        offset: u64,
        index_type: vk::IndexType,
    ) -> Result<(), CommandBufferTransitionError> {
        if self.get_current_state() != CommandBufferState::Recording {
            return Err(CommandBufferTransitionError::WrongState);
        }

        unsafe {
            self.device
                .vko()
                .cmd_bind_index_buffer(*self.vko(), *buffer.vko(), offset, index_type);
        }

        self.add_dependent_buffer(buffer);

        Ok(())
    }

    ///Clears a color image to a supplied `clear_color_value`. Notes that this takes some time. If you know that the whole image will be written
    /// to by a pass, consider to not clear the image before.
    pub fn cmd_clear_color_image(
        &self,
        image: Arc<dyn AbstractImage + Send + Sync>,
        image_layout: vk::ImageLayout,
        clear_color_value: vk::ClearColorValue,
        ranges: Vec<vk::ImageSubresourceRange>,
    ) -> Result<(), CommandBufferTransitionError> {
        if self.get_current_state() != CommandBufferState::Recording {
            return Err(CommandBufferTransitionError::WrongState);
        }

        unsafe {
            self.device.vko().cmd_clear_color_image(
                *self.vko(),
                *image.as_vk_image(),
                image_layout,
                &clear_color_value,
                ranges.as_slice(),
            );
        }

        self.add_dependent_image(image);
        self.set_graphics();
        Ok(())
    }

    ///Clears a depth_stencil image to a supplied `clear_value`. Notes that this takes some time. If you know that the whole image will be written
    /// to by a pass, consider to not clear the image before.
    pub fn cmd_clear_depth_stencil_image(
        &self,
        image: Arc<dyn AbstractImage + Send + Sync>,
        image_layout: vk::ImageLayout,
        clear_value: vk::ClearDepthStencilValue,
        ranges: Vec<vk::ImageSubresourceRange>,
    ) -> Result<(), CommandBufferTransitionError> {
        if self.get_current_state() != CommandBufferState::Recording {
            return Err(CommandBufferTransitionError::WrongState);
        }

        unsafe {
            self.device.vko().cmd_clear_depth_stencil_image(
                *self.vko(),
                *image.as_vk_image(),
                image_layout,
                &clear_value,
                ranges.as_slice(),
            );
        }

        self.add_dependent_image(image);
        self.set_graphics();
        Ok(())
    }

    ///Clears a attachments which is bound to the current Renderpass. You must use this function between a call to `cmd_begin_renderpass` and `end_renderpass`.
    pub fn cmd_clear_attachments(
        &self,
        attachments: Vec<vk::ClearAttachment>,
        rects: Vec<vk::ClearRect>,
    ) -> Result<(), CommandBufferTransitionError> {
        if self.get_current_state() != CommandBufferState::Recording {
            return Err(CommandBufferTransitionError::WrongState);
        }

        unsafe {
            self.device.vko().cmd_clear_attachments(
                *self.vko(),
                attachments.as_slice(),
                rects.as_slice(),
            );
        }
        self.set_graphics();
        Ok(())
    }

    ///Draws a shape based on the currently bound index/vertex buffer and the suplied information.
    /// Make sure that index and vertex buffers are bound and that the supplied information are within the ranges of those.
    pub fn cmd_draw_indexed(
        &self,
        index_count: u32,
        instance_count: u32,
        first_index: u32,
        vertex_offset: i32,
        first_instance: u32,
    ) -> Result<(), CommandBufferTransitionError> {
        if self.get_current_state() != CommandBufferState::Recording {
            return Err(CommandBufferTransitionError::WrongState);
        }

        unsafe {
            self.device.vko().cmd_draw_indexed(
                *self.vko(),
                index_count,
                instance_count,
                first_index,
                vertex_offset,
                first_instance,
            );
        }

        Ok(())
    }

    ///Draws based on parameters given in `buffer`. This is good if you want to generate drawing commands on the gpu instead of the cpu.
    pub fn cmd_draw_indexed_indirect(
        &self,
        indirect_buffer: Arc<Buffer>,
        offset: u64,
        draw_count: u32,
        stride: u32,
    ) -> Result<(), CommandBufferTransitionError> {
        if self.get_current_state() != CommandBufferState::Recording {
            return Err(CommandBufferTransitionError::WrongState);
        }

        unsafe {
            self.device.vko().cmd_draw_indexed_indirect(
                *self.vko(),
                *indirect_buffer.vko(),
                offset,
                draw_count,
                stride,
            );
        }

        self.add_dependent_buffer(indirect_buffer);

        Ok(())
    }

    ///Draws a shape based on the currently bound vertex buffer and the suplied information.
    /// Make sure that the vertex buffer ist bound and that the supplied information are within the ranges of it.
    pub fn cmd_draw(
        &self,
        vertex_count: u32,
        instance_count: u32,
        first_vertex: u32,
        first_instance: u32,
    ) -> Result<(), CommandBufferTransitionError> {
        if self.get_current_state() != CommandBufferState::Recording {
            return Err(CommandBufferTransitionError::WrongState);
        }

        unsafe {
            self.device.vko().cmd_draw(
                *self.vko(),
                vertex_count,
                instance_count,
                first_vertex,
                first_instance,
            );
        }

        Ok(())
    }

    ///Draws based on parameters given in `buffer`. This is good if you want to generate drawing commands on the gpu instead of the cpu.
    pub fn cmd_draw_indirect(
        &self,
        indirect_buffer: Arc<Buffer>,
        offset: u64,
        draw_count: u32,
        stride: u32,
    ) -> Result<(), CommandBufferTransitionError> {
        if self.get_current_state() != CommandBufferState::Recording {
            return Err(CommandBufferTransitionError::WrongState);
        }

        unsafe {
            self.device.vko().cmd_draw_indirect(
                *self.vko(),
                *indirect_buffer.vko(),
                offset,
                draw_count,
                stride,
            );
        }

        self.add_dependent_buffer(indirect_buffer);

        Ok(())
    }

    ///Executes a secondary command buffer as part of this primary command buffer.
    pub fn cmd_execute_commands(
        &self,
        secondary_command_buffer: Vec<Arc<CommandBuffer>>,
    ) -> Result<(), CommandBufferTransitionError> {
        if self.get_current_state() != CommandBufferState::Recording {
            return Err(CommandBufferTransitionError::WrongState);
        }

        let cbs: Vec<_> = secondary_command_buffer
            .iter()
            .map(|cb| *cb.vko())
            .collect();

        unsafe {
            self.device.vko().cmd_execute_commands(*self.vko(), &cbs);
        }

        for (arc_cb, inner_cb) in secondary_command_buffer.into_iter().zip(cbs.into_iter()) {
            self.add_dependend_resources(CommandBufferResources::SecondaryCommandBuffer {
                command_buffer: arc_cb,
                inner: inner_cb,
            });
        }

        Ok(())
    }

    ///Binds a descriptorsets from binding index `first_set` until `first_set + descriptor_sets.len()`
    pub fn cmd_bind_descriptor_sets(
        &self,
        pipeline_bind_point: vk::PipelineBindPoint,
        layout: Arc<PipelineLayout>,
        first_set: u32,
        descriptor_sets: Vec<Arc<DescriptorSet>>,
        dynamic_offsets: Vec<u32>,
    ) -> Result<(), CommandBufferTransitionError> {
        if self.get_current_state() != CommandBufferState::Recording {
            return Err(CommandBufferTransitionError::WrongState);
        }

        let desc_sets: Vec<_> = descriptor_sets.iter().map(|s| *s.vko()).collect();

        unsafe {
            self.device.vko().cmd_bind_descriptor_sets(
                *self.vko(),
                pipeline_bind_point,
                *layout.vko(),
                first_set,
                desc_sets.as_slice(),
                &dynamic_offsets,
            );
        }

        for (desc_set, inner_desc_set) in descriptor_sets.into_iter().zip(desc_sets.into_iter()) {
            self.add_dependend_resources(CommandBufferResources::DescriptorSet {
                desc_set,
                inner: inner_desc_set,
            });
        }
        self.add_dependend_resources(CommandBufferResources::PipelineLayout(layout));

        Ok(())
    }

    pub fn cmd_push_constants<T: Sized + 'static>(
        &self,
        layout: Arc<PipelineLayout>,
        constant: &PushConstant<T>,
    ) -> Result<(), CommandBufferTransitionError> {
        if self.get_current_state() != CommandBufferState::Recording {
            return Err(CommandBufferTransitionError::WrongState);
        }

        unsafe {
            self.device.vko().cmd_push_constants(
                *self.vko(),
                *layout.vko(),
                constant.get_stage(),
                0, //We currently don't support offsets
                constant.content_as_bytes(),
            );
        }

        self.add_dependend_resources(CommandBufferResources::PipelineLayout(layout));

        Ok(())
    }

    ///Starts the renderpass supplied.
    pub fn cmd_begin_render_pass(
        &self,
        render_pass: Arc<RenderPass>,
        framebuffer: Arc<Framebuffer>,
        render_area: vk::Rect2D,
        clear_values: Vec<vk::ClearValue>,
        subpass_contents: vk::SubpassContents,
    ) -> Result<(), CommandBufferTransitionError> {
        if self.get_current_state() != CommandBufferState::Recording {
            return Err(CommandBufferTransitionError::WrongState);
        }

        let start_info = vk::RenderPassBeginInfo::builder()
            .render_pass(*render_pass.vko())
            .framebuffer(*framebuffer.vko())
            .render_area(render_area)
            .clear_values(clear_values.as_slice());

        unsafe {
            self.device
                .vko()
                .cmd_begin_render_pass(*self.vko(), &start_info, subpass_contents);
        }

        self.add_dependend_resources(CommandBufferResources::RenderPass {
            render_pass,
            framebuffer,
            //begin_info: Arc::new(start_info.build()) //TODO Check if that's okay
        });
        self.set_graphics();
        Ok(())
    }

    ///Changes to the next subpass on this renderpass.
    pub fn cmd_next_subpass(
        &self,
        subpass_contents: vk::SubpassContents,
    ) -> Result<(), CommandBufferTransitionError> {
        if self.get_current_state() != CommandBufferState::Recording {
            return Err(CommandBufferTransitionError::WrongState);
        }

        unsafe {
            self.device
                .vko()
                .cmd_next_subpass(*self.vko(), subpass_contents);
        }
        self.set_graphics();
        Ok(())
    }

    ///Ends the current renderpass.
    pub fn cmd_end_render_pass(&self) -> Result<(), CommandBufferTransitionError> {
        if self.get_current_state() != CommandBufferState::Recording {
            return Err(CommandBufferTransitionError::WrongState);
        }

        unsafe {
            self.device.vko().cmd_end_render_pass(*self.vko());
        }
        self.set_graphics();
        Ok(())
    }

    ///Binds `pipeline` to the command buffer. This pipeline will be used until the comannd buffer has finished recording or another pipeline is bound.
    pub fn cmd_bind_pipeline(
        &self,
        bind_point: vk::PipelineBindPoint,
        pipeline: Arc<dyn Pipeline + Send + Sync>,
    ) -> Result<(), CommandBufferTransitionError> {
        if self.get_current_state() != CommandBufferState::Recording {
            return Err(CommandBufferTransitionError::WrongState);
        }

        unsafe {
            self.device
                .vko()
                .cmd_bind_pipeline(*self.vko(), bind_point, *pipeline.vko());
        }

        self.add_dependend_resources(CommandBufferResources::Pipeline(pipeline));

        Ok(())
    }

    ///Binds a vertex buffer which will be used with all following draw commands except another buffer
    ///is bound or the command buffer finishes recording.
    ///For every buffer an offset into that buffer has to be specified.
    pub fn cmd_bind_vertex_buffers(
        &self,
        first_binding: u32,
        buffers: Vec<(Arc<Buffer>, u64)>,
    ) -> Result<(), CommandBufferTransitionError> {
        if self.get_current_state() != CommandBufferState::Recording {
            return Err(CommandBufferTransitionError::WrongState);
        }

        let (mut bufs, mut arc_bufs, mut offsets) = (Vec::new(), Vec::new(), Vec::new());
        for (b, o) in buffers {
            bufs.push(*b.vko());
            arc_bufs.push(b);
            offsets.push(o);
        }

        unsafe {
            self.device.vko().cmd_bind_vertex_buffers(
                *self.vko(),
                first_binding,
                bufs.as_slice(),
                offsets.as_slice(),
            );
        }

        for buffer in arc_bufs {
            self.add_dependend_resources(CommandBufferResources::Buffer(buffer));
        }
        //Add offsets as well?

        Ok(())
    }

    pub fn cmd_dispatch(
        &self,
        group_count_xyz: [u32; 3],
    ) -> Result<(), CommandBufferTransitionError> {
        if self.get_current_state() != CommandBufferState::Recording {
            return Err(CommandBufferTransitionError::WrongState);
        }

        unsafe {
            self.device.vko().cmd_dispatch(
                *self.vko(),
                group_count_xyz[0],
                group_count_xyz[1],
                group_count_xyz[2],
            );
        }

        Ok(())
    }

    pub fn cmd_dispatch_indirect(
        &self,
        buffer: Arc<Buffer>,
        offset: u64,
    ) -> Result<(), CommandBufferTransitionError> {
        if self.get_current_state() != CommandBufferState::Recording {
            return Err(CommandBufferTransitionError::WrongState);
        }

        unsafe {
            self.device
                .vko()
                .cmd_dispatch_indirect(*self.vko(), *buffer.vko(), offset);
        }

        self.add_dependent_buffer(buffer);

        Ok(())
    }

    ///Sets the current scissors. Has no effect if you didn't specify the Scissors on the currently used pipeline's viewport with a empty Vec.
    pub fn cmd_set_scissors(
        &self,
        scissors: Vec<vk::Rect2D>,
    ) -> Result<(), CommandBufferTransitionError> {
        if self.get_current_state() != CommandBufferState::Recording {
            return Err(CommandBufferTransitionError::WrongState);
        }

        unsafe {
            self.device.vko().cmd_set_scissor(
                *self.vko(),
                0, //Alawys using all scissors
                scissors.as_slice(),
            );
        }

        Ok(())
    }

    /// Sets the line width used when drawin primitivs in line mode. Has no effect if you did not set the
    /// `line_width` with a None value while creating the currently used pipeline.
    pub fn cmd_set_line_width(&self, width: f32) -> Result<(), CommandBufferTransitionError> {
        if self.get_current_state() != CommandBufferState::Recording {
            return Err(CommandBufferTransitionError::WrongState);
        }

        unsafe {
            self.device.vko().cmd_set_line_width(*self.vko(), width);
        }

        Ok(())
    }

    pub fn cmd_set_viewport(
        &self,
        viewports: Vec<vk::Viewport>,
    ) -> Result<(), CommandBufferTransitionError> {
        if self.get_current_state() != CommandBufferState::Recording {
            return Err(CommandBufferTransitionError::WrongState);
        }

        unsafe {
            self.device.vko().cmd_set_viewport(
                *self.vko(),
                0, //Always use all of them
                viewports.as_slice(),
            );
        }

        Ok(())
    }
    pub fn cmd_set_depth_bias(
        &self,
        constant_factor: f32,
        clamp: f32,
        slope_factor: f32,
    ) -> Result<(), CommandBufferTransitionError> {
        if self.get_current_state() != CommandBufferState::Recording {
            return Err(CommandBufferTransitionError::WrongState);
        }

        unsafe {
            self.device
                .vko()
                .cmd_set_depth_bias(*self.vko(), constant_factor, clamp, slope_factor);
        }

        Ok(())
    }
    pub fn cmd_set_blend_constants(
        &self,
        constants: [f32; 4],
    ) -> Result<(), CommandBufferTransitionError> {
        if self.get_current_state() != CommandBufferState::Recording {
            return Err(CommandBufferTransitionError::WrongState);
        }

        unsafe {
            self.device
                .vko()
                .cmd_set_blend_constants(*self.vko(), &constants);
        }

        Ok(())
    }

    pub fn cmd_set_depth_bounds(
        &self,
        min_bound: f32,
        max_bound: f32,
    ) -> Result<(), CommandBufferTransitionError> {
        if self.get_current_state() != CommandBufferState::Recording {
            return Err(CommandBufferTransitionError::WrongState);
        }

        unsafe {
            self.device
                .vko()
                .cmd_set_depth_bounds(*self.vko(), min_bound, max_bound);
        }

        Ok(())
    }
    pub fn cmd_set_stencil_compare_mask(
        &self,
        face_mask: vk::StencilFaceFlags,
        compare_mask: u32,
    ) -> Result<(), CommandBufferTransitionError> {
        if self.get_current_state() != CommandBufferState::Recording {
            return Err(CommandBufferTransitionError::WrongState);
        }

        unsafe {
            self.device
                .vko()
                .cmd_set_stencil_compare_mask(*self.vko(), face_mask, compare_mask);
        }

        Ok(())
    }
    pub fn cmd_set_stencil_write_mask(
        &self,
        face_mask: vk::StencilFaceFlags,
        write_mask: u32,
    ) -> Result<(), CommandBufferTransitionError> {
        if self.get_current_state() != CommandBufferState::Recording {
            return Err(CommandBufferTransitionError::WrongState);
        }

        unsafe {
            self.device
                .vko()
                .cmd_set_stencil_write_mask(*self.vko(), face_mask, write_mask);
        }

        Ok(())
    }
    pub fn cmd_set_stencil_reference(
        &self,
        face_mask: vk::StencilFaceFlags,
        reference: u32,
    ) -> Result<(), CommandBufferTransitionError> {
        if self.get_current_state() != CommandBufferState::Recording {
            return Err(CommandBufferTransitionError::WrongState);
        }

        unsafe {
            self.device
                .vko()
                .cmd_set_stencil_reference(*self.vko(), face_mask, reference);
        }

        Ok(())
    }
    pub fn cmd_write_time_stamp(
        &self,
        pipeline_stage: vk::PipelineStageFlags,
        query_pool: vk::QueryPool,
        query: u32,
    ) -> Result<(), CommandBufferTransitionError> {
        if self.get_current_state() != CommandBufferState::Recording {
            return Err(CommandBufferTransitionError::WrongState);
        }

        unsafe {
            self.device
                .vko()
                .cmd_write_timestamp(*self.vko(), pipeline_stage, query_pool, query);
        }

        //TODO use warpped pool and add here.

        Ok(())
    }

    ///Places a memory barrier. Make sure that all specified memory/image/buffer resources are free at the specified pipeline stage and that
    /// you try to transition as many resource at once as possible.
    ///
    ///Have a look at [this](https://gpuopen.com/vulkan-barriers-explained/) for an excellent description how this works best.
    pub fn cmd_pipeline_barrier(
        &self,
        src_stage_mask: vk::PipelineStageFlags,
        dst_stage_mask: vk::PipelineStageFlags,
        dependency_flags: vk::DependencyFlags,
        memory_barriers: Vec<vk::MemoryBarrier>,
        buffer_barriers: Vec<vk::BufferMemoryBarrier>,
        image_barriers: Vec<vk::ImageMemoryBarrier>,
    ) -> Result<(), CommandBufferTransitionError> {
        if self.get_current_state() != CommandBufferState::Recording {
            return Err(CommandBufferTransitionError::WrongState);
        }

        unsafe {
            self.device.vko().cmd_pipeline_barrier(
                *self.vko(),
                src_stage_mask,
                dst_stage_mask,
                dependency_flags,
                memory_barriers.as_slice(),
                buffer_barriers.as_slice(),
                image_barriers.as_slice(),
            );
        }

        /*
        for b in memory_barriers{
            self.add_dependend_resources(CommandBufferResources::MemoryBarrier(b))
        }
        for b in buffer_barriers{
            self.add_dependend_resources(CommandBufferResources::BufferBarrier(b))
        }
        for b in image_barriers{
            self.add_dependend_resources(CommandBufferResources::ImageBarrier(b))
        }
        */
        Ok(())
    }

    ///Creates the pipeline barrier from a barrier builder. This high-level builder is usually the right way of building barriers.
    /// If you need finer control use [cmd_pipeline_barrier](CommandBuffer::cmd_pipeline_barrier) for the vulkan way of defining barriers.
    pub fn pipeline_barrier<const N: usize>(
        &self,
        barrier: PipelineBarrier<N>,
    ) -> Result<(), CommandBufferTransitionError> {
        if self.get_current_state() != CommandBufferState::Recording {
            return Err(CommandBufferTransitionError::WrongState);
        }

        unsafe {
            self.device.vko().cmd_pipeline_barrier(
                *self.vko(),
                barrier.src_stage,
                barrier.dst_stage,
                barrier.dependecies,
                barrier.memory_barriers.as_slice(),
                barrier.buffer_barriers.as_slice(),
                barrier.image_barriers.as_slice(),
            );
        }

        Ok(())
    }

    ///Create this label and returns its handle
    pub(crate) fn add_label(&self, name: &str, color: Option<[f32; 4]>) -> DebugUtilsLabelEXT {
        let mut resouces = self.resources.write().unwrap();
        let mut data = name.to_string();
        //Add null
        data.push_str("\0");
        resouces.push(CommandBufferResources::DebugLabel(data));

        //Now build the label
        match resouces.last().unwrap() {
            CommandBufferResources::DebugLabel(st) => DebugUtilsLabelEXT::builder()
                .label_name(CStr::from_bytes_with_nul(st.as_bytes()).unwrap())
                .color(color.unwrap_or([1.0, 1.0, 1.0, 1.0]))
                .build(),
            _ => panic!("Label was not a label..."),
        }
    }

    //TODO expose all command buffer commands and record state to panic on invalid record
    /*
    cmd_copy_query_pool_results
    cmd_begin_query
    cmd_end_query
    cmd_reset_query_pool
    */
}

impl Drop for CommandBuffer {
    fn drop(&mut self) {
        //Delocate the command buffer
        self.pool.free_command_buffer(self);
    }
}
