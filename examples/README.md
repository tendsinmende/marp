# marp examples
## Content
The examples ususally use the marps wrappers util module to generate a standart pipeline as well as most of the normal 
vulkan objects.

The only exception is the triangle example, which takles care of everything.

## Examples
- Triangle: creates a simple triangle from a vertex and index buffer. Write directly to the swapchain and a depth image. Runs for 1000 frames, then closes.
- Cube: Creates a cube with vertex colors
- Cube Textures: Creates a spinning cube, which has a texture applied to it.
- Push Constants: Creates a triangle where the color is updated through a push constant.
