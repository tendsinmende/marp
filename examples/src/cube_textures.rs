/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

extern crate cgmath;
extern crate image as img;

use cgmath::*;
use marp::buffer::*;
use marp::memory::*;
use marp::pipeline::*;
use marp::sampler::*;
use marp::*;
use marp::{ash::vk, debug::Debugger};

use marp_surface_winit::winit;
use marp_utils::*;

use std::mem;
use std::sync::Arc;
use std::u64;
use std::{ffi::CStr, time::Instant};

//Our Vertex format
#[derive(Clone, Debug, Copy)]
#[repr(align(16))] //Needed since in glsl everything is 16bit aligned.
struct Vertex {
    pos: [f32; 4],
    color: [f32; 4],
    uv: [f32; 2],
}

#[repr(C)]
#[derive(Clone, Copy, Debug)]
struct UniformBufferObject {
    model: [[f32; 4]; 4],
    view: [[f32; 4]; 4],
    proj: [[f32; 4]; 4],
}

pub fn main() {
    let events_loop = winit::event_loop::EventLoop::new();
    let window = winit::window::Window::new(&events_loop).expect("Failed to create window!");

    //Create a device and queue that support the surface and the graphics operations
    let (device, queue, surface) = create_device_and_gfx_queue(&window, &events_loop)
        .expect("Failed to create device and queue");

    //This one descibes how vertices are bound to the shader.
    let vertex_state = pipeline::VertexInputState {
        vertex_binding_descriptions: vec![vk::VertexInputBindingDescription::builder()
            .binding(0)
            .stride(mem::size_of::<Vertex>() as u32)
            .input_rate(vk::VertexInputRate::VERTEX)
            .build()],
        vertex_attribute_descriptions: vec![
            //Description of the Pos attribute
            vk::VertexInputAttributeDescription::builder()
                .location(0)
                .binding(0)
                .format(vk::Format::R32G32B32A32_SFLOAT)
                .offset(offset_of!(Vertex, pos) as u32)
                .build(),
            //Description of the Color attribute
            vk::VertexInputAttributeDescription::builder()
                .location(1)
                .binding(0)
                .format(vk::Format::R32G32B32A32_SFLOAT)
                .offset(offset_of!(Vertex, color) as u32)
                .build(),
            //The Uv attribute
            vk::VertexInputAttributeDescription::builder()
                .location(2)
                .binding(0)
                .format(vk::Format::R32G32_SFLOAT)
                .offset(offset_of!(Vertex, uv) as u32)
                .build(),
        ],
    };

    //Create Vertex/Index buffer
    let indices = vec![
        0, 1, 2, 0, 2, 3, //front
        4, 5, 6, 4, 6, 7, //right
        8, 9, 10, 8, 10, 11, //back
        12, 13, 14, 12, 14, 15, //left
        16, 17, 18, 16, 18, 19, //upper
        20, 21, 22, 20, 22, 23,
    ]; //, 0, 3, 2, 0, 1, 4];
    let (index_upload, index_buffer) = marp::buffer::Buffer::from_data(
        device.clone(),
        queue.clone(),
        indices.clone(),
        BufferUsage {
            index_buffer: true,
            ..Default::default()
        },
        buffer::SharingMode::Exclusive,
        MemoryUsage::GpuOnly, //we only want device access for this buffer.
    )
    .expect("Failed to create index buffer");
    index_upload
        .wait(u64::MAX)
        .expect("Failed to upload inidice buffer!");

    let vertices = vec![
        //front
        Vertex {
            pos: [-1.0, -1.0, 1.0, 1.0],
            color: [0.0, 0.0, 1.0, 1.0],
            uv: [0.0, 0.0],
        },
        Vertex {
            pos: [1.0, -1.0, 1.0, 1.0],
            color: [1.0, 0.0, 1.0, 1.0],
            uv: [1.0, 0.0],
        },
        Vertex {
            pos: [1.0, 1.0, 1.0, 1.0],
            color: [1.0, 1.0, 1.0, 1.0],
            uv: [1.0, 1.0],
        },
        Vertex {
            pos: [-1.0, 1.0, 1.0, 1.0],
            color: [0.0, 1.0, 1.0, 1.0],
            uv: [0.0, 1.0],
        },
        //right
        Vertex {
            pos: [1.0, 1.0, 1.0, 1.0],
            color: [1.0, 1.0, 1.0, 1.0],
            uv: [1.0, 1.0],
        },
        Vertex {
            pos: [1.0, 1.0, -1.0, 1.0],
            color: [1.0, 1.0, 0.0, 1.0],
            uv: [1.0, 0.0],
        },
        Vertex {
            pos: [1.0, -1.0, -1.0, 1.0],
            color: [1.0, 0.0, 0.0, 1.0],
            uv: [0.0, 0.0],
        },
        Vertex {
            pos: [1.0, -1.0, 1.0, 1.0],
            color: [1.0, 0.0, 1.0, 1.0],
            uv: [0.0, 1.0],
        },
        //back
        Vertex {
            pos: [-1.0, -1.0, -1.0, 1.0],
            color: [0.0, 0.0, 0.0, 1.0],
            uv: [0.0, 0.0],
        },
        Vertex {
            pos: [1.0, -1.0, -1.0, 1.0],
            color: [1.0, 0.0, 0.0, 1.0],
            uv: [1.0, 0.0],
        },
        Vertex {
            pos: [1.0, 1.0, -1.0, 1.0],
            color: [1.0, 1.0, 0.0, 1.0],
            uv: [1.0, 1.0],
        },
        Vertex {
            pos: [-1.0, 1.0, -1.0, 1.0],
            color: [0.0, 1.0, 0.0, 1.0],
            uv: [0.0, 1.0],
        },
        //left
        Vertex {
            pos: [-1.0, -1.0, -1.0, 1.0],
            color: [0.0, 0.0, 0.0, 1.0],
            uv: [0.0, 0.0],
        },
        Vertex {
            pos: [-1.0, -1.0, 1.0, 1.0],
            color: [0.0, 0.0, 1.0, 1.0],
            uv: [0.0, 1.0],
        },
        Vertex {
            pos: [-1.0, 1.0, 1.0, 1.0],
            color: [0.0, 1.0, 1.0, 1.0],
            uv: [1.0, 1.0],
        },
        Vertex {
            pos: [-1.0, 1.0, -1.0, 1.0],
            color: [0.0, 1.0, 0.0, 1.0],
            uv: [1.0, 0.0],
        },
        //up
        Vertex {
            pos: [1.0, 1.0, 1.0, 1.0],
            color: [1.0, 1.0, 1.0, 1.0],
            uv: [1.0, 1.0],
        },
        Vertex {
            pos: [-1.0, 1.0, 1.0, 1.0],
            color: [0.0, 1.0, 1.0, 1.0],
            uv: [0.0, 1.0],
        },
        Vertex {
            pos: [-1.0, 1.0, -1.0, 1.0],
            color: [0.0, 1.0, 0.0, 1.0],
            uv: [0.0, 0.0],
        },
        Vertex {
            pos: [1.0, 1.0, -1.0, 1.0],
            color: [1.0, 1.0, 0.0, 1.0],
            uv: [1.0, 0.0],
        },
        //bottom
        Vertex {
            pos: [-1.0, -1.0, -1.0, 1.0],
            color: [0.0, 0.0, 0.0, 1.0],
            uv: [0.0, 0.0],
        },
        Vertex {
            pos: [1.0, -1.0, -1.0, 1.0],
            color: [1.0, 0.0, 0.0, 1.0],
            uv: [1.0, 0.0],
        },
        Vertex {
            pos: [1.0, -1.0, 1.0, 1.0],
            color: [1.0, 0.0, 1.0, 1.0],
            uv: [1.0, 1.0],
        },
        Vertex {
            pos: [-1.0, -1.0, 1.0, 1.0],
            color: [0.0, 0.0, 1.0, 1.0],
            uv: [0.0, 1.0],
        },
    ];
    let (vertex_upload, vertex_buffer) = marp::buffer::Buffer::from_data(
        device.clone(),
        queue.clone(),
        vertices, //The buffer size we need
        BufferUsage {
            vertex_buffer: true,
            ..Default::default()
        },
        buffer::SharingMode::Exclusive,
        MemoryUsage::GpuOnly, //we only want device access for this buffer.
    )
    .expect("Failed to create index buffer");
    vertex_upload
        .wait(u64::MAX)
        .expect("Failed to upload vertex buffer!");

    let desc_data = UniformBufferObject {
        model: Matrix4::identity().into(),
        view: Matrix4::look_at(
            Point3::new(4.0, 4.0, -3.0),
            Point3::new(0.0, 0.0, 0.0),
            Vector3::new(0.0, 0.0, 1.0),
        )
        .into(),
        proj: perspective(Deg(45.0), 21.0 / 9.0 as f32, 0.1 as f32, 50.0 as f32).into(),
    };

    //we create only one image buffer, since this will be immutable and therefore not be written
    //from multiple queues.
    //We just bind it to multiple descriptor sets.
    let image = img::open("examples/resources/img.png")
        .expect("Failed to load image")
        .to_rgba();
    let image_dimensions = image.dimensions();
    let image_data = image.into_raw();

    println!("Image_info: {}x{}", image_dimensions.0, image_dimensions.1);

    let image = image::Image::new(
        device.clone(),
        image::ImageInfo::new(
            image::ImageType::Image2D {
                width: image_dimensions.0,
                height: image_dimensions.1,
                samples: 1,
            },
            vk::Format::R8G8B8A8_UNORM,
            None,                               //No extra ordinary component mapping
            Some(image::MipLevel::Specific(1)), //Only one mip map for now
            image::ImageUsage {
                sampled: true,
                transfer_dst: true,
                color_attachment: true,
                ..Default::default()
            },
            MemoryUsage::GpuOnly,
            None, //Optimal tiling by default
        ),
        SharingMode::Exclusive,
    )
    .expect("Failed to create image");

    //Now copy our data to the GPUOnly image
    image
        .copy(queue.clone(), image_data)
        .expect("Failed to upload texture to gpu");

    println!("Finished image!");
    //Create a descriptor set for each frame in flight. All of them have the same layout,
    //so we can bind them all to the same pipeline.
    let mut desc_set_and_buffer = Vec::new();
    for _i in 0..FRAMES_IN_FLIGHT {
        let (uniform_upload, uniform_buffer) = marp::buffer::Buffer::from_data(
            device.clone(),
            queue.clone(),
            vec![desc_data], //The buffer size we need
            BufferUsage {
                uniform_buffer: true,
                ..Default::default()
            },
            buffer::SharingMode::Exclusive,
            MemoryUsage::CpuToGpu, //we only want device access for this buffer.
        )
        .expect("Failed to create index buffer");
        uniform_upload
            .wait(u64::MAX)
            .expect("Failed to upload vertex buffer!");

        let sampler = SamplerBuilder::default()
            .build(device.clone())
            .expect("Failed to create sampler!");

        //Create the descriptorset for our shader
        let descriptor_set = descriptor::create_static_descriptor_set(
            device.clone(),
            vec![
                descriptor::DescResource::new_buffer(
                    0,
                    vec![uniform_buffer.clone()],
                    vk::DescriptorType::UNIFORM_BUFFER,
                ),
                descriptor::DescResource::new_image(
                    1,
                    vec![(
                        image.clone(),
                        Some(sampler),
                        vk::ImageLayout::SHADER_READ_ONLY_OPTIMAL,
                    )],
                    vk::DescriptorType::COMBINED_IMAGE_SAMPLER,
                ),
            ],
        )
        .expect("Failed to create static descriptor set");

        desc_set_and_buffer.push((descriptor_set, uniform_buffer));
    }

    //create debugger
    let debugger = Debugger::new(device.clone());
    if debugger.is_some() {
        println!("Loaded debugger!");
    } else {
        println!("Could not load debugger!");
    }
    //Since we have all data for the pipeline ready, start the test bed.
    let base_app = RenderBed::new(
        device.clone(),
        queue.clone(),
        surface,
        window,
        events_loop,
        "examples/shader/cube_textures/vert.spv",
        "examples/shader/cube_textures/frag.spv",
        vec![*desc_set_and_buffer[0].0.layout()], //The descriptor set layouts we want to take with our pipeline
        vec![],                                   //push constants,
        vertex_state,
    );

    let start_time = Instant::now();
    println!("Created Cube base app!");

    let layout = base_app.pipeline.layout();
    base_app.start(move |(idx, cb)| {
        if let Some(db) = &debugger {
            db.begin_region(cb.clone(), "Main Frame", Some([1.0, 1.0, 0.0, 1.0]))
        }

        //Update the uniform buffer
        update_uniform_buffer(
            desc_set_and_buffer[idx].1.clone(),
            queue.clone(),
            start_time,
        );

        cb.cmd_bind_vertex_buffers(0, vec![(vertex_buffer.clone(), 0)])
            .expect("Failed to bind vertex buffer!");

        cb.cmd_bind_index_buffer(index_buffer.clone(), 0, vk::IndexType::UINT32)
            .expect("Failed to bind index buffer!");

        cb.cmd_bind_descriptor_sets(
            vk::PipelineBindPoint::GRAPHICS,
            layout.clone(),
            0,
            vec![desc_set_and_buffer[idx].0.clone()], //Check if we need more?
            vec![],
        )
        .expect("Failed to bind descriptor sets");

        if let Some(db) = &debugger {
            db.add_label(cb.clone(), "Drawin Cube!", Some([0.0, 1.0, 0.0, 1.0]))
        }

        cb.cmd_draw_indexed(indices.len() as u32, 1, 0, 0, 1)
            .expect("Faild to draw primitives");

        if let Some(db) = &debugger {
            db.end_region(cb.clone());
        }

        cb
    });
}

fn update_uniform_buffer(
    buffer: Arc<Buffer>,
    _queue: Arc<device::queue::Queue>,
    start_time: Instant,
) {
    let elapsed = (start_time.elapsed().as_millis() as f32 / 4000.0) % 1.0;

    let trans = Decomposed {
        scale: 1.0 as f32,
        rot: Quaternion::from_angle_z(Deg((elapsed * 360.0) as f32)),
        disp: Vector3::new(0.0, 0.0, 0.0),
    };

    let mat: Matrix4<f32> = trans.into();

    let desc_data = UniformBufferObject {
        model: mat.into(),

        view: Matrix4::look_at(
            Point3::new(4.0, 4.0, -3.0),
            Point3::new(0.0, 0.0, 0.0),
            Vector3::new(0.0, 0.0, 1.0),
        )
        .into(),

        proj: perspective(Deg(45.0), 21.0 / 9.0 as f32, 0.1 as f32, 50.0 as f32).into(),
    };

    buffer
        .map_data(vec![desc_data])
        .expect("Failed to map data to buffer!");
}
