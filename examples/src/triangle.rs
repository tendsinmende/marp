/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

//!This examples will (at some point) be able to render a single triangle using
//! ash + winit
use marp::ash::vk;
use marp::buffer::*;
use marp::command_buffer::CommandPool;
use marp::device::queue::*;
use marp::image::AbstractImage;
use marp::memory::*;
use marp::shader::*;
use marp::*;

use marp::swapchain::surface::Surface;

use marp_surface_winit::winit;
use marp_surface_winit::WinitSurface;

use std::sync::Arc;
use std::u64;

use std::mem;

//Our Vertex format
#[derive(Clone, Debug, Copy)]
#[repr(C)]
struct Vertex {
    pos: [f32; 4],
    color: [f32; 4],
}

// Simple offset_of macro akin to C++ offsetof
#[macro_export]
macro_rules! offset_of {
    ($base:path, $field:ident) => {{
        #[allow(unused_unsafe)]
        unsafe {
            let b: $base = mem::uninitialized();
            (&b.$field as *const _ as isize) - (&b as *const _ as isize)
        }
    }};
}

pub fn main() {
    //Create an window and an instance
    let mut app_info = miscellaneous::AppInfo::default("MyApp".to_string());
    app_info.set_api_version(miscellaneous::Version::new(1, 1, 0));

    let extensions = miscellaneous::InstanceExtensions::presentable();
    let layers = vec![
        miscellaneous::InstanceLayer::new("VK_LAYER_KHRONOS_validation")
            .expect("Could not create instance extension!"),
    ];

    let eventsloop = winit::event_loop::EventLoop::new();
    let window = winit::window::Window::new(&eventsloop).expect("Failed to create window");

    let instance =
        match instance::Instance::new(Some(app_info), Some(extensions), Some(layers), None) {
            Ok(inst) => inst,
            Err(er) => {
                println!("failed to create instance: {}", er);
                return;
            }
        };

    //Create a surface for the window
    let surface: Arc<dyn Surface + Send + Sync> =
        WinitSurface::new(instance.clone(), &window, &eventsloop).expect("Failed to create window");

    //Create a physical device
    let p_devices = device::physical_device::PhysicalDevice::find_physical_device(instance.clone())
        .expect("failed to find physical devices");
    //find the physical device that supports presentation on out surface.
    let (p_device, index) = p_devices
        .into_iter()
        .filter_map(|pdev| match pdev.find_present_queue_family(&surface) {
            Ok(queue_idx) => {
                println!("Found suitable physical device");
                Some((pdev, queue_idx))
            }
            Err(er) => {
                println!("Could not find presentable surface for dev: {:?}", er);
                None
            }
        })
        .nth(0)
        .expect("failed to find suitable device at all");

    //Create one queue for the
    let queue_create_info = vec![(
        *p_device
            .get_queue_family_by_index(index)
            .expect("Failed to get graphics queue family"),
        1.0 as f32,
    )];
    let needed_device_ext = vec![
        miscellaneous::DeviceExtension::new("VK_KHR_swapchain".to_string(), 1),
        //miscellaneous::DeviceExtension::new("VK_EXT_debug_marker".to_string(), 1)
    ];
    let (device, mut queues) = device::device::Device::new(
        instance.clone(),
        p_device.clone(),
        queue_create_info,
        Some(&needed_device_ext),
        Some(*p_device.get_features()),
    )
    .expect("failed to find Device and its queues");
    println!("Got {}, present queues", queues.len());
    let queue = queues.pop().expect("Could not get our present queue!");

    //Find a depth and color format
    let swapchain_format = surface
        .get_supported_formats(device.get_physical_device())
        .expect("Failed to get surface formats")
        .into_iter()
        .nth(0)
        .expect("failed to get first swapchain format");
    let depth_format = device
        .get_useable_format(
            vec![
                vk::Format::D32_SFLOAT_S8_UINT,
                vk::Format::D24_UNORM_S8_UINT,
                vk::Format::D16_UNORM_S8_UINT,
            ],
            &image::ImageUsage {
                depth_attachment: true,
                input_attachment: true,
                ..Default::default()
            },
            ash::vk::ImageTiling::OPTIMAL,
        )
        .expect("Could not find a depth format");

    let window_extent = window.inner_size();
    let swapchain_dimension = vk::Extent2D::builder()
        .width(window_extent.width as u32)
        .height(window_extent.height as u32)
        .build();

    //Create swapchain
    let swapchain = swapchain::Swapchain::new(
        device.clone(),
        surface,
        swapchain_dimension,
        Some(swapchain_format),
        Some(2),
        Some(ash::vk::PresentModeKHR::IMMEDIATE),
        Some(image::ImageUsage {
            color_attachment: true,
            ..Default::default()
        }),
    )
    .expect("Failed to create swapchain!");

    swapchain
        .images_to_present_layout(queue.clone())
        .wait(std::u64::MAX)
        .expect("Failed to transition swapchain images to present layout!");

    let extent = swapchain.get_extent();

    swapchain
        .images_to_present_layout(queue.clone())
        .wait(u64::MAX)
        .expect("Could not wait for swapchain images");

    //Get swapchain images and create a depth image.
    //The swapchain images are handled by the swapchain so we get only a reference.
    let swapchain_images = swapchain.get_images();

    let depth_image_info = image::ImageInfo::new(
        marp::image::ImageType::Image2D {
            width: extent.width,
            height: extent.height,
            samples: 1,
        },
        depth_format,
        None,
        Some(image::MipLevel::Specific(1)),
        image::ImageUsage {
            depth_attachment: true,
            input_attachment: true,
            ..Default::default()
        },
        MemoryUsage::GpuOnly,
        None,
    );

    let depth_image = marp::image::Image::new(
        device.clone(),
        depth_image_info,
        buffer::SharingMode::Exclusive,
    )
    .expect("Failed to create depth image!");
    // TODO transition to DepthStencilAttachment_read/write access mask, and DepthStencilOptimal Layout

    //Setup the renderpass we want to use while rendering the triangle
    let attachment_descs = vec![
        //The color attachment
        framebuffer::AttachmentDescription::new(
            swapchain_images[0].image_type().sample_count_flag(),
            swapchain_images[0].format(),
            ash::vk::AttachmentStoreOp::STORE,
            ash::vk::AttachmentLoadOp::CLEAR,
            None, // no stencil store
            None, // no stencil load
            None, //No initial layout needed since we clear anyways
            ash::vk::ImageLayout::PRESENT_SRC_KHR,
        ),
        //The depth image
        depth_image.to_attachment_description(
            ash::vk::AttachmentLoadOp::CLEAR,
            ash::vk::AttachmentStoreOp::DONT_CARE,
        ),
    ];

    //This are the subpasses this renderpass goes through. However, this is easy in a triangle example.
    let render_pass = framebuffer::RenderPass::new(attachment_descs)
        .add_subpass(vec![
            framebuffer::AttachmentRole::OutputColor,
            framebuffer::AttachmentRole::DepthStencil,
        ])
        .add_dependency(framebuffer::SubpassDependency::new(
            ash::vk::SUBPASS_EXTERNAL,                            //from external pass
            0,                                                    //To first and only subpass
            ash::vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT, //When color output has finished on src
            ash::vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT, //Color output can start on this one
            ash::vk::AccessFlags::empty(),                        //Don't know access type of source
            ash::vk::AccessFlags::COLOR_ATTACHMENT_READ
                | ash::vk::AccessFlags::COLOR_ATTACHMENT_WRITE, //We need color read and write support
        ))
        .add_dependency(framebuffer::SubpassDependency::new(
            0,
            ash::vk::SUBPASS_EXTERNAL,
            ash::vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT,
            ash::vk::PipelineStageFlags::BOTTOM_OF_PIPE,
            ash::vk::AccessFlags::COLOR_ATTACHMENT_READ
                | ash::vk::AccessFlags::COLOR_ATTACHMENT_WRITE, //Don't know access type of source
            ash::vk::AccessFlags::empty(),
        ))
        .build(device.clone())
        .expect("Failed to build renderpass!");

    //TODO setup the frambuffers, one per swapchain image
    let mut framebuffers = Vec::new();
    for idx in 0..swapchain_images.len() {
        framebuffers.push(
            framebuffer::Framebuffer::new(
                device.clone(),
                render_pass.clone(),
                vec![swapchain_images[idx].clone(), depth_image.clone()],
            )
            .expect("Failed to create Framebuffer!"),
        );
    }

    //Signals when a presentation is done
    let present_compleat =
        sync::Semaphore::new(device.clone()).expect("failed to create acquire semaphore");
    //Signals when rendering has finished
    let rendering_complete =
        sync::Semaphore::new(device.clone()).expect("Failed to create semaphore!");

    //Setup the command pool and allocate a command buffer for each frame on the swapchain
    let command_pool = command_buffer::CommandBufferPool::new(
        device.clone(),
        queue.clone(),
        ash::vk::CommandPoolCreateFlags::RESET_COMMAND_BUFFER,
    )
    .expect("Failed to create command pool");

    let mut command_buffers = command_pool
        .alloc(swapchain_images.len(), false)
        .expect("Failed to allocate command buffers"); //Allocate a primary command buffer for each frame/swapchain image.

    //TODO Create Vertex/Index buffer
    let indices: Vec<u32> = vec![0, 1, 2]; //, 0, 3, 2, 0, 1, 4];
    let (index_upload, index_buffer) = marp::buffer::Buffer::from_data(
        device.clone(),
        queue.clone(),
        indices.clone(), //The buffer size we need
        BufferUsage {
            index_buffer: true,
            storage_buffer: true,
            ..Default::default()
        },
        buffer::SharingMode::Exclusive,
        MemoryUsage::GpuOnly, //we only want device access for this buffer.
    )
    .expect("Failed to create index buffer");
    index_upload
        .wait(u64::MAX)
        .expect("Failed to upload inidice buffer!");

    let vertices = vec![
        Vertex {
            pos: [-0.9, 0.9, 0.0, 0.9],
            color: [0.0, 1.0, 0.0, 1.0],
        },
        Vertex {
            pos: [0.9, 0.9, 0.0, 0.9],
            color: [0.0, 0.0, 1.0, 1.0],
        },
        Vertex {
            pos: [0.0, -0.9, 0.0, 0.9],
            color: [1.0, 0.0, 0.0, 1.0],
        },
        Vertex {
            pos: [-0.9, -0.9, 0.0, 0.9],
            color: [1.0, 0.0, 0.0, 1.0],
        },
    ];

    println!(
        "Size of vertices: {}",
        (vertices.len() * std::mem::size_of::<Vertex>())
    );

    let (vertex_upload, vertex_buffer) = marp::buffer::Buffer::from_data(
        device.clone(),
        queue.clone(),
        vertices, //The buffer size we need
        BufferUsage {
            vertex_buffer: true,
            ..Default::default()
        },
        buffer::SharingMode::Exclusive,
        MemoryUsage::GpuOnly, //we only want device access for this buffer.
    )
    .expect("Failed to create index buffer");
    vertex_upload
        .wait(u64::MAX)
        .expect("Failed to upload vertex buffer!");

    //Load Shaders
    let vertex_shader =
        ShaderModule::new_from_spv(device.clone(), "examples/shader/triangle/vert.spv")
            .expect("Failed to load vertex shader");
    let fragment_shader =
        ShaderModule::new_from_spv(device.clone(), "examples/shader/triangle/frag.spv")
            .expect("Failed to load fragment shader");

    let vertex_stage = vertex_shader.to_stage(Stage::Vertex, "main");
    let fragment_stage = fragment_shader.to_stage(Stage::Fragment, "main");

    // setup Graphicspipeline
    //This one descibes how vertices are bound to the shader.
    let vertex_state = pipeline::VertexInputState {
        vertex_binding_descriptions: vec![vk::VertexInputBindingDescription::builder()
            .binding(0)
            .stride(mem::size_of::<Vertex>() as u32)
            .input_rate(vk::VertexInputRate::VERTEX)
            .build()],
        vertex_attribute_descriptions: vec![
            //Description of the Pos attribute
            vk::VertexInputAttributeDescription::builder()
                .location(0)
                .binding(0)
                .format(vk::Format::R32G32B32A32_SFLOAT)
                .offset(offset_of!(Vertex, pos) as u32)
                .build(),
            //Description of the Color attribute
            vk::VertexInputAttributeDescription::builder()
                .location(1)
                .binding(0)
                .format(vk::Format::R32G32B32A32_SFLOAT)
                .offset(offset_of!(Vertex, color) as u32)
                .build(),
        ],
    };

    //This describes how the input (the vertices) should be interpreted
    let input_assembly_state = pipeline::InputAssemblyState {
        topology: vk::PrimitiveTopology::TRIANGLE_LIST,
        restart_enabled: false,
    };

    let viewport = pipeline::Viewport {
        corner_xy: [0.0, 0.0],
        width_height: [extent.width as f32, extent.height as f32],
        min_max_depth: [0.0, 1.0],
    };

    //TODO scissor
    let scissors = pipeline::Scissor {
        offset: [0, 0],
        extent: extent,
    };
    //TODO vieport_state
    let viewport_state =
        pipeline::ViewportState::new(pipeline::ViewportMode::Static(vec![(viewport, scissors)]));
    //TODO raster_info
    let raster_info = pipeline::RasterizationState {
        depth_clamp_enabled: false,
        rasterizer_discard_enabled: false,
        polygone_mode: vk::PolygonMode::FILL,
        cull_mode: vk::CullModeFlags::NONE, //No culling for now
        front_face: vk::FrontFace::COUNTER_CLOCKWISE,
        line_width: Some(2.0),
        depth_bias: None, //No depth bias for now
    };

    //TODO multisample_state
    let multisample_state = pipeline::MultisampleState {
        sample_count: vk::SampleCountFlags::TYPE_1,
        sample_shading: false,
        min_sample_shading: 1.0,
        alpha_to_coverage: false,
        alpha_to_one: false,
    };
    //TODO depth_stencil_state
    let depth_stencil_state = pipeline::DepthStencilState {
        depth_test_enabled: true,
        depth_write_enabled: true,
        depth_compare_op: vk::CompareOp::ALWAYS,
        stencil_test: pipeline::StencilTestState::NoTest,
        depth_bounds: None,
    };
    //TODO color_blend
    let color_blend_state = pipeline::ColorBlendState::new(
        Some([0.0, 0.0, 0.0, 0.0]),
        Some(vk::LogicOp::COPY),
        vec![
            //Our Color attachment
            pipeline::ColorBlendAttachmentState::NoBlending,
        ],
    );

    //Create pipeline
    let pipeline_state = pipeline::PipelineState::new(
        vertex_state,
        input_assembly_state,
        None,
        viewport_state,
        raster_info,
        multisample_state,
        depth_stencil_state,
        Some(color_blend_state),
    );

    let pipeline_layout = pipeline::PipelineLayout::new(device.clone(), vec![], vec![])
        .expect("Failed to create GraphicsPipelineLayout");

    let pipeline = pipeline::GraphicsPipeline::new(
        device.clone(),
        vec![vertex_stage.clone(), fragment_stage.clone()],
        pipeline_state,
        pipeline_layout,
        render_pass.clone(),
        0, //Use subpass 0
    )
    .expect("Failed to create Graphics pipeline");

    //TODO Record commandbuffers
    for (idx, cb) in command_buffers.iter_mut().enumerate() {
        cb.begin_recording(false, false, true, None)
            .expect("Failed to begin renderpass");

        let clear_values = vec![
            vk::ClearValue {
                color: vk::ClearColorValue {
                    float32: [0.0, 0.0, 0.0, 0.0],
                },
            },
            vk::ClearValue {
                depth_stencil: vk::ClearDepthStencilValue {
                    depth: 1.0,
                    stencil: 0,
                },
            },
        ];

        cb.cmd_begin_render_pass(
            render_pass.clone(),
            framebuffers[idx].clone(),
            vk::Rect2D {
                offset: vk::Offset2D { x: 0, y: 0 },
                extent: extent,
            },
            clear_values,
            vk::SubpassContents::INLINE,
        )
        .expect("Failed to start renderpass");

        cb.cmd_bind_pipeline(vk::PipelineBindPoint::GRAPHICS, pipeline.clone())
            .expect("Failed to bind pipeline");

        cb.cmd_bind_index_buffer(index_buffer.clone(), 0, vk::IndexType::UINT32)
            .expect("Failed to bind index buffer!");

        cb.cmd_bind_vertex_buffers(0, vec![(vertex_buffer.clone(), 0)])
            .expect("Failed to bind vertex buffer!");

        cb.cmd_draw_indexed(
            indices.len() as u32,
            1, //One instance
            0, //first index
            0, //No vertex offset
            1, //First instance
        )
        .expect("Failed to draw primitive!");

        cb.cmd_end_render_pass().expect("Failed to end renderpass!");
        cb.end_recording().expect("Failed to end cb recording");
    }

    let i = 0;

    //Start event loop and shedule all events
    eventsloop.run(move |event, _, control_flow| {
        *control_flow = winit::event_loop::ControlFlow::Poll;

        match event {
            winit::event::Event::WindowEvent { event, .. } => {
                match event {
                    winit::event::WindowEvent::CloseRequested => {
                        *control_flow = winit::event_loop::ControlFlow::Exit
                    }
                    winit::event::WindowEvent::KeyboardInput { input, .. } => {
                        match input.virtual_keycode {
                            Some(winit::event::VirtualKeyCode::Escape) => {
                                *control_flow = winit::event_loop::ControlFlow::Exit
                            }
                            _ => (),
                        }
                    }
                    _ => {} //Other window events
                }
            }
            winit::event::Event::MainEventsCleared => window.request_redraw(),
            winit::event::Event::RedrawRequested(_) => {
                let swapchain_image_idx = swapchain
                    .acquire_next_image(u64::MAX, present_compleat.clone())
                    .expect("Failed to get next image");

                let render_fence = queue
                    .queue_submit(vec![SubmitInfo::new(
                        vec![(
                            present_compleat.clone(),
                            vk::PipelineStageFlags::COLOR_ATTACHMENT_OUTPUT,
                        )], //Waits for this aka. when the image is ready
                        vec![command_buffers[i % command_buffers.len()].clone()],
                        vec![rendering_complete.clone()], //Signals that rending is done and we can use the image
                    )])
                    .expect("Failed to submit work to queue!");

                //Wait for the rendering to be finished
                render_fence
                    .wait(u64::MAX)
                    .expect("Failed to wait for renderer!");

                swapchain
                    .queue_present(
                        queue.clone(),
                        vec![rendering_complete.clone()],
                        swapchain_image_idx,
                    )
                    .expect("Failed to wait for image");
            }
            _ => {} //All other events
        }
    });

    //sleep(Duration::from_secs(4));
}
//TODO find a better way to handle thode pointer issues
