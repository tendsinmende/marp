/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#version 450
//#extension GL_ARB_separate_shader_objects : enable
//#extension GL_ARB_shading_language_420pack : enable

layout (location = 0) in vec4 o_color;
layout (location= 1) in vec2 uv;
layout (location = 0) out vec4 uFragColor;

layout(set = 0, binding = 1) uniform sampler2D image;

void main() {
  uFragColor = texture(image, uv);
}
