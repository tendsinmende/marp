/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/. */

#version 450
//#extension GL_ARB_separate_shader_objects : enable
//#extension GL_ARB_shading_language_420pack : enable

layout (location = 0) in vec4 pos;

layout (push_constant) uniform PC{
  vec4 color;
  float x;
  float y;
}pc;

layout (location = 0) out vec4 o_color;
void main() {
  o_color = pc.color;
  vec4 p = pos;
  p.x = p.x + pc.x;
  p.y = p.y + pc.y;
  
  gl_Position = p;
}
